# PUG

*Plasticity Umat Generator*

Abaqus UMAT modular framework for the MetPlast project at NTNU/PhysMet.
The code is based on [NTNU-PhysMet's continuum plasticity](https://gitlab.com/ntnu-physmet/continuum-plasticity) (T. Manik, B. Holmedal) and the corresponding paper [[1]](#References).

**WHAT IT DOES**
- Generate custom Abaqus/umat for *elasto-plasticity*.
- User choice includes *elastic model*, *yield function*, *hardening type* (isotropic, kinematic, distortional), etc.
- Generate sample *input files* for customization.
- New models can easily be added and documented.

[[_TOC_]]

## Installation

*To do: define requirements and provide setup.py*

## Usage

### With the graphical user interface (GUI)

```bash
cd umat-metplast
python lib/client/gui_tkinter.py
```
Choose options, click `Apply` and `Generate`. Done!
The umat file `PUG.f` and the input sample `PUG.inp` will be created in the current directory.

### With the command line interface (CLI)

Generate a default configuration file.
```bash
python lib/client -g MyProject.yaml
```
Then modify `MyProject.yaml` with the desired options.

Once customized, generate a UMAT using the CLI:
```bash
python lib/client MyProject.yaml
```
The umat file `PUG.f` appears in the current directory.
It contains *relative inclusions* of the model components, hence cannot be moved or copied elsewhere.
The input file sample `PUG.inp` is also generated with default values.

To generate a monolithic file containing all the definitions and no inclusions, use the `export` option:
```bash
python lib/client --export MyProject.yaml
```

### Abaqus/Umat

Run Abaqus with, for example, 
```bash
cd examples
abaqus job=JOBNAME input=tension user=../PUG interactive
```

**Warning**: the input files under `examples/` contain a relative inclusion pointing to the root directory:
```fortran
*INCLUDE, input=../PUG.inp
```
Make sure that the location of `PUG.inp` is consistent when computing examples.


## Notes

### Further details

This file contains *user* information.
For *developer* information, see [`CONTRIBUTING.md`](CONTRIBUTING.md).

### Existing projects

From [awesome-cae](https://github.com/WeilinDeng/ABAQUS):
- [ABAQUS-US]() - A variety of ABAQUS user element (UELs) and user material (UMATs) subroutines
- [ABAQUS_Subroutines](https://github.com/ALandauer/ABAQUS_Subroutines) - Viscoplastic UMAT, rotational body force UEL, UMAT implmenentation of Linder et al. 2011
- [ABAQUS_Subroutines 2](https://github.com/WeilinDeng/ABAQUS) - collection of abaqus user materials

More repositories:
- Excellent/Plasticity: [MaterialModel](https://github.com/KnutAM/MaterialModels)
- Nice README: [ABQ_PDALAC](https://github.com/ammarkh95/ABAQUS_PDALAC)
- to check: [UMAT-ABAQUS](https://github.com/jpsferreira/UMAT-ABAQUS)

## References

[1] Manik, T., 2021. A natural vector/matrix notation 
     applied in an efficient and robust return-mapping algorithm for 
     advanced yield functions, Eur J Mech a-Solid, Volume 90,
     doi.org/10.1016/j.euromechsol.2021.104357
