c=======================================================================
c These lines are for the c preprocessor
c     >>> gfortran -cpp -DLSMAX=20
c ----------------------------------------------------------------------
c RES_SIZE is the target size of the residual (psi_tol)
#ifndef RES_SIZE
#define RES_SIZE 1.e-20
#endif
c ----------------------------------------------------------------------
c BEST_FIRST_GUESS is to optimize the newton initial stress guess
c#define BEST_FIRST_GUESS
c GUESS_SCALE is to scale the firstguess
#ifndef GUESS_SCALE
#define GUESS_SCALE 1.0
#endif
c ----------------------------------------------------------------------
c LS_MAX is the maximum number 
#ifndef LS_MAX
#define LS_MAX 50
#endif
c=======================================================================

      SUBROUTINE Umock( stress, d_strain, d_t,
     +                  statev, ns, props,nprops, 
     +                  ddsdde
     +)

      ! I/O
      integer ns, infor, nprops
      real*8 stress(6), d_strain(6), d_t, statev(ns), props(nprops)
      real*8 ddsdde(6,6)

      ! other
      real*8 algmod(6,6)
      real*8 svec(6), dstrain(6)
C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) properties splitting
C             |_|         (_____|
C-----------------------------------------------------------------------
      integer np_EL, np_HT, np_ES, np_RS
      real*8  par_EL(2), par_RS(3), par_ES(19), par_HT(0)
      np_EL = 2  !
      np_RS = 3  !
      np_ES = 19  !
      np_HT = 0  !
      call Props_Assign(PROPS, NPROPS, par_EL, np_EL, 0                )
      call Props_Assign(PROPS, NPROPS, par_RS, np_RS, np_EL            )
      call Props_Assign(PROPS, NPROPS, par_ES, np_ES, np_EL+np_RS      )
      call Props_Assign(PROPS, NPROPS, par_HT, np_HT, np_EL+np_RS+np_ES)
C_______________________________________________________________________

c ------------ From ABAQUS to natural basis ---------------------------
      call voigt_to_natural(STRESS, svec, D_STRAIN, dstrain)

c ------------ THE CORE ROUTINE - RETURN-MAPPING ALGORITHM ------------

      call Return_Map(svec, dstrain, d_t,
     +                STATEV, NS,
     +                par_HT, np_HT, 
     +                par_EL, np_EL, 
     +                par_ES, np_ES, 
     +                par_RS, np_RS, 
     +                algmod, infor)

c ------------ From natural basis back to abaqus Voigt ----------------
      call natural_to_voigt(STRESS, svec, algmod, ddsdde)

      RETURN

C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) log format 200
C             |_|         (_____|
C-----------------------------------------------------------------------
200   format(/
     +        5x, '***********************************'/,
C    +        5x, '*   _    _ __  __       _______   *'/,
C    +        5x, '*  | |  | |  \/  |   /\|__   __|  *'/,
C    +        5x, '*  | |  | | \  / |  /  \  | |     *'/,
C    +        5x, '*  | |  | | |\/| | / /\ \ | |     *'/,
C    +        5x, '*  | |__| | |  | |/ ____ \| |     *'/,
C    +        5x, '*   \____/|_|  |_/_/    \_\_|     *'/,
C    +        5x, '*                                 *'/,
C    +        5x, '***********************************'/,
C    +        5x, '     in the natural notation       '/,
C    +        5x, '                                   '/,
C    +        5x, '              WITH                 '/,
C    +        5x, '      MATERIAL PARAMETERS:         '/,
C    +        5x, '                                   '/,
     +        5x, ' prop:    = ',               1pe12.5/,
     +        5x, '***********************************'
     +  /)
C_______________________________________________________________________
C

      END SUBROUTINE Umock

C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) file inclusions
C             |_|         (_____|
C-----------------------------------------------------------------------
      include "../../lib/umat/common/linear_algebra.f"
      include "../../lib/umat/common/voigt_to_natural.f"
      include "../../lib/umat/common/utils.f"
      include "../../lib/umat/elasticity/isotropic.f"
      !include "../../lib/umat/hardening_type/isotropic_gen.f"
      include "../../lib/umat/equivalent_stress/yld2004.f"
      include "../../lib/umat/reference_stress/voce.f"
C_______________________________________________________________________
C

C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) GET PROPS! GET STATEV!
C             |_|         (_____|
C-----------------------------------------------------------------------

      subroutine     GET_SIZES_PS(NP, NS)
        implicit none
        integer NP, NS
        NS=1
        NP=24
      end subroutine GET_SIZES_PS

      subroutine assign_props(PROPS, NP)
        implicit none
        integer NP
        real*8 PROPS(NP)
        props=(/
     +    70000.0, 0.33,
     +    20.0, 150.0, 0.2,
     +    8.0,
     +    0.0 , 0.0, 0.73, 0.134, 0.134, 0.736, 0.922, 0.637, 0.901,
     +    0.0 , 0.0, 0.73, 0.134, 0.134, 0.736, 0.922, 0.637, 0.901
     +    /)
      end subroutine assign_props


C_______________________________________________________________________



C> @file isotropic_gen.f 
C>    Two internal variables named
C>    - accumulated plastic strain denoted \f$p\f$.
C>    - smth that conjugates with X

      subroutine Return_Map(svec, dstrain, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      implicit none  

      ! (in/out)
      integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      real*8   svec(6), dstrain(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      integer  i, i_max, j, j_max, k, l
      real*8   sig_trial(5), sig_dev(5), acp, d_lam,  ! state vars
     +         d_sig_dev(5), d_acp, dd_lam,         ! increments
     +         d_eps_p(5), d_sig(6),                        ! others
     +         sig_ref, eqs, eqs_grad(5), eqs_hess(5,5), h, !yld fun
     +         res_f, res_e(5), res_p, dim_sig,
     +         L_inv(5,5), C_inv(6,6),
     +         choldiag(5), cholres(5),
     +         psi_0, psi, psi_tol, alpha, eta, beta  ! algo

      real*8 ls_sum, t_end, t_start
      
      CALL CPU_TIME(T_START)

      i = 0
      j = 0


      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      psi_tol = RES_SIZE
      eta = 1.d-1
      beta = 1.d-4
      i_max = 1000
      j_max = LS_MAX

      ls_sum=0.

      !=================================================================
      ! SPLIT ARRAYS
      !-----------------------------------------------------------------

      acp   = state_vars(1)
      d_eps_p = 0.d0!dstrain(2:6)

      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      call dStrain_to_dStress(dstrain, par_EL, d_sig)
      sig_trial = svec(2:6) + d_sig(2:6)

      ! Evaluate the yield function  f = eqs - sig_ref
      call Reference_Stress(par_RS, acp, sig_ref)
      call Equivalent_Stress(sig_trial, par_ES, 1,
     +                       eqs, eqs_grad, eqs_hess)

      ! Tangent operators
      call Tangent_Compliance(par_EL, C_inv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)

      !write(6,*) "# TEST: ELASTIC"
      !write(*,*) "> START: compare", eqs, " to ", sig_ref, "at", acp

      IF (eqs.LE.sig_ref) THEN
      !IF (.TRUE.) THEN

        alpha = 1.d0
        d_sig_dev = sig_trial - svec(2:6)
        sig_dev = sig_trial
        d_acp     = 0.d0
        dd_lam    = 0.d0

        !write(6,*) "# ELASTIC STEP"

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

        !write(6,*) "# PLASTIC STEP"

        ! Initialize newton iteration
#ifdef BEST_FIRST_GUESS
        sig_dev = sig_trial*sig_ref/eqs * GUESS_SCALE
#else
        sig_dev = sig_trial
#endif

        d_sig_dev = sig_dev - svec(2:6)

        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_lam   = sum(d_eps_p*sig_dev)/sig_ref
        acp     = acp  + d_lam

        d_acp   = d_lam!sum(d_eps_p*sig_dev)/sig_ref
        dd_lam  = d_lam


  
        !! Compute residual

        call Reference_Stress(par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)
c BAP ABOVE  Not in Tomas code, does not change much
        call Equivalent_Stress(sig_dev,!+d_sig_dev, 
     +                         par_ES, 3,
     +                         eqs, eqs_grad, eqs_hess)
        call Residual( sig_dev,d_lam,        ! main variables
     +                 d_eps_p,d_acp,        ! increments
     +                 eqs,eqs_grad,sig_ref, ! other vars
     +                 dim_sig,              ! params
     +                 res_f, res_e, res_p,  ! resisuals
     +                 psi_0)                ! merit function



        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        ! NEWTON START

        i = 0

        !write(*,*) i, "go  Newt-loop with psi0=", psi_0
        !psi_0 = max(psi_0, psi_tol*1.001)
        !write(*,*) i, "go  Newt-loop with psi0=", psi_0

        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))
        !do while (i .LE. i_max)

          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory

          ! ............................................................
          ! Initializations
          i = i+1
  
          ! Get Linv and prepare choleski things
          L_inv = C_inv(2:6,2:6) + d_lam*eqs_hess
          ! (lower triangle of Linv will be modified but the upper
          ! triangle incl. the diagonal will not)
          call chol_decomp(L_inv, 5, choldiag)

          ! ............................................................
          ! Inversion 

          call Reference_Stress(par_RS, acp, sig_ref)
          call Reference_Stress_Deriv(par_RS, acp, h)

          ! Multiplier
          call chol_solve(L_inv, 5, eqs_grad, choldiag, cholres)
          dd_lam = (
     +      res_f + h*res_p
     +      - sum(cholres*res_e)
     +    )/(
     +      h+ sum(cholres*eqs_grad)
     +    )

          d_acp = res_p + dd_lam
          !d_acp = dd_lam

          ! Stress
          call chol_solve(L_inv, 5,
     +                    res_e+eqs_grad*dd_lam,
     +                    choldiag, cholres
     +                    )
          d_sig_dev = - cholres

          ! ............................................................
          ! Residual (U+DU)
          call dStress_to_dStrain_dev(sig_trial-sig_dev-d_sig_dev,
     +                                par_EL, d_eps_p)
          call Equivalent_Stress(sig_dev+d_sig_dev, 
     +                           par_ES, 3,
     +                           eqs, eqs_grad, eqs_hess)
          call Reference_Stress(par_RS, acp+d_acp, sig_ref)
          call Residual(sig_dev+d_sig_dev,       ! main variables
     +                  d_lam+dd_lam,            ! ... + a+dx
     +                  d_eps_p,d_acp,     ! increments
     +                  eqs,eqs_grad, sig_ref,   ! other vars
     +                  dim_sig,                 ! params
     +                  res_f, res_e, res_p,     ! resisuals
     +                  psi)  

          ! ............................................................
          ! Line search 

          alpha=1.
          !write(*,*) i, "G0 LS with  psi=", psi

          j = 0

          do while ((psi .GT. (1.d0-2.d0*beta*alpha)*psi_0) 
     +             .and. (j .LT. j_max))
            ! entering this loop means alpha=1. is not satisfactory.
            ! Let us adjust it and test over and over
            j = j+1
            ls_sum = ls_sum + 1.d0

            alpha = max(eta*alpha,
     +                  psi_0*alpha**2/(psi-psi_0*(1.-2.*alpha)))

            !! Compute residual U+alpha*DU
            call dStress_to_dStrain_dev(
     +                        sig_trial-sig_dev-alpha*d_sig_dev,
     +                        par_EL, d_eps_p)
            call Equivalent_Stress(sig_dev+alpha*d_sig_dev, 
     +                             par_ES, 3,
     +                             eqs, eqs_grad, eqs_hess)
            call Reference_Stress(par_RS, acp+alpha*d_acp, sig_ref)
            call Residual(sig_dev+alpha*d_sig_dev,        ! main variables
     +                    d_lam  +alpha*dd_lam,           ! ... + a+dx
     +                    d_eps_p,alpha*d_acp,  ! increments
     +                    eqs,eqs_grad, sig_ref,          ! other vars
     +                           dim_sig,                 ! params
     +                    res_f, res_e, res_p,     ! resisuals
     +                    psi)
            !write(*,*) j, "     in LS with  psi=", psi, "a=",alpha
          enddo
          ! ............................................................
          ! Final merit update

          psi_0 = psi
          !write(*,*) i,"in  Newt-loop with psi0=",psi_0,"alpha=",alpha

          ! --------------------------------------------------------------
          ! Final state variables updates
          ! --------------------------------------------------------------

          sig_dev = sig_dev + alpha * d_sig_dev
          d_lam   = d_lam   + alpha * dd_lam
          acp     = acp     + alpha * d_acp

        enddo                                                                   ! NEWTON STOP

        !d_sig(1) = d_sig(1)
        !d_sig(2:6) = sigd_sig_dev

        !write(*,*) i, "out Newt-loop with  psi=", psi
        !write(*,*) "> END:  compare", eqs, " to ", sig_ref, "at", acp

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------
        
        ! For now algmod = C. Adjust it!

        call Reference_Stress_Deriv(par_RS, acp, h)

        L_inv  = C_inv(2:6,2:6) + eqs_hess*d_lam
        call chol_decomp(L_inv, 5, choldiag)
        call chol_solve(L_inv, 5, eqs_grad, choldiag, cholres)

        call chol_inverse(L_inv, 5, choldiag, algmod(2:6,2:6))

        do k=1,5
          do l=1,5
            algmod(1+k,1+l) =
     +        + algmod(1+k,1+l)
     +        - cholres(k) * cholres(l) / (h+sum(cholres*eqs_grad))
          enddo
        enddo

      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------

      svec(1) = svec(1)+d_sig(1)
      svec(2:6) = sig_dev

      state_vars(1)   = acp

      CALL CPU_TIME(T_END)
      write(*,*) "CONVERGENCE", t_end-t_start,",", i, ",",ls_sum/(i+1)

      RETURN

      end subroutine Return_Map





C#######################################################################




C-----------------------------------------------------------------------
C> @brief residual

      subroutine Residual(sig_dev,d_lam,        ! main variables
     +                    d_eps_p,d_acp,        ! increments
     +                    eqs,eqs_grad,sig_ref, ! other vars
     +                    dim_sig,              ! params
     +                    res_f, res_e, res_p,  ! resisuals
     +                    merit_fun)            ! psi
        implicit none

        real*8 sig_dev(5), d_lam,
     +         d_eps_p(5),d_acp,
     +         eqs,eqs_grad(5),sig_ref,      
     +         dim_sig,            
     +         res_f, res_e(5), res_p,
     +         merit_fun                 

        ! Compute residuals individually 
        res_f = eqs-sig_ref
        res_e = d_lam*eqs_grad - d_eps_p
        res_p = 0.d0!d_lam - d_acp!
        ! /!\ RES_P is WRONG, d_lam cannot be compared to
        !     d_acp (incremental d_lam and iterative d_acp)

        ! Compute The merit function with normalized residuals
        merit_fun  = (
     +          res_f**2/dim_sig**2
     +          + sum(res_e*res_e)
     +          + res_p**2
     +          )

      end subroutine Residual

      
C-----------------------------------------------------------------------
C> @brief Jacobian
      subroutine Jacobian(sig_dev,X,d_lam,            ! main variables
     +                    h, eqs_grad, eqs_hess,
     +                    eps_x, dim_sig,
     +                    Cinv,
     +                    jac)                  
        implicit none
        real*8        sig_dev(5),X(5),d_lam,            ! main variables
     +                h, eqs_grad(5), eqs_hess(5,5),
     +                eps_x, dim_sig,
     +                Cinv(5,5),
     +                jac(12,12)

        !jac = 0.

        !jac( 1   , 6   ) = -h
        !jac( 1   , 7:11) = eqs_grad

        !jac( 2: 6, 1: 5) = Cinv
        !jac( 2: 6, 7:11) = eqs_hess*d_lam
        !jac( 2: 6,   12) = eqs_grad

        !jac(    7,    6) = -1.
        !jac(    7,   12) = +1.

        !jac(    8,    1) =  -1.
        !jac(    9,    2) =  -1.
        !jac(   10,    3) =  -1.
        !jac(   11,    4) =  -1.
        !jac(   12,    5) =  -1.

        !jac(    8,    7) =  1. + d_lam/eps_x
        !jac(    9,    8) =  1. + d_lam/eps_x
        !jac(   10,    9) =  1. + d_lam/eps_x
        !jac(   11,   10) =  1. + d_lam/eps_x
        !jac(   12,   11) =  1. + d_lam/eps_x

        !jac( 8:12,   12) =  (sig_dev - X)/eps_x

        RETURN
      end subroutine Jacobian
