""" This scripts plot out the result of the bash generator
In order to make the results more readable and comparable,
some cloud of points are reduced in size.
"""
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import itertools

CASE_LIST = (
    "cro",
    "bau",
)
GS_LIST = (
    "g0s0",
    "g0s1",
    "g1s0",
    "g1s1",
)
UMOCK_LIST = (
    "iso",
    "zie",
    "hol",
)

FILE_STR = "{gs}_{case}_{umock}"  # .format(3e-4, 1e-12)

NEW_LEN = 30
HEADERS = {
    "CONV": "inc_time\tavg_Newt_iters\tavg_LS_iters",
    "HARD": "strain\tstress",
}


def coarsen_aray(arr, new_len):
    """Average packs of value in given array
    Arguments
    ---------
    arr:
        numpy array of shape (N,M) to coarsen
    new len : int
        length of the new averaged array (<N)

    Returns
    -------
    new_array: ndarray
        numpy array of shape (new_len,M) after coarsening
    """
    reste = len(arr) % new_len
    new_arr = np.zeros(arr.shape)[:new_len]
    grp_size = int(len(arr) / new_len)
    row_start = 0
    row_end = 0
    for i_grp in range(new_len):
        row_start = row_end
        row_end += 1 + grp_size if i_grp < reste else grp_size
        new_arr[i_grp] = np.mean(arr[row_start:row_end], axis=0)
    return new_arr


for case in CASE_LIST:
    FIG_HARD = go.Figure()
    FIG_CONV = go.Figure()
    for gs, umock in itertools.product(
        GS_LIST,
        UMOCK_LIST,
    ):
        basename = FILE_STR.format(case=case, gs=gs, umock=umock)
        print(basename)
        data = pd.read_csv('dat/compare/'+basename + "_variables.csv").values
        if data.dtype == "O":
            continue
        FIG_HARD.add_trace(
            go.Scatter(
                x=data[:, 0],
                y=data[:, 1],
                name=str(basename),
                mode="markers",
            )
        )
        data = pd.read_csv('dat/compare/'+basename + "_convergence.csv").values
        newd = coarsen_aray(data, NEW_LEN)
        # FIG_CONV.add_trace(go.Scatter(x=(data[:,0]),y=np.log(data[:,1]),name=str(basename),
        #                              mode='markers'))
        FIG_CONV.add_trace(
            go.Scatter(x=(newd[:, 0]), y=(newd[:, 1]), name=str(basename),mode="markers")
        )
        np.savetxt(
                'dat/PGF_DATA/convpare_'+basename+'.txt',
                newd,
                fmt='%.3e',
                header = HEADERS['CONV'],
                )
    FIG_CONV.show()
    FIG_HARD.show()
