""" This scripts plot out the result of the bash generator
In order to make the results more readable and comparable,
some cloud of points are reduced in size.
"""
import pandas as pd
import numpy as np
import plotly.graph_objects as go

NEW_LEN = 50
HEADERS = {
    "CONV": "inc_time\tavg_Newt_iters\tavg_LS_iters",
    "HARD": "strain\tstress",
}

CASE="ten"
#UMOCK="iso"#
UMOCK="zie"#
#UMOCK="hol"#

SCALE_LIST = np.arange(.1, 2.3, .1)


def coarsen_aray(arr, new_len):
    """Average packs of value in given array
    Arguments
    ---------
    arr:
        numpy array of shape (N,M) to coarsen
    new len : int
        length of the new averaged array (<N)

    Returns
    -------
    new_array: ndarray
        numpy array of shape (new_len,M) after coarsening
    """
    reste = len(arr) % new_len
    new_arr = np.zeros(arr.shape)[:new_len]
    grp_size = int(len(arr) / new_len)
    row_start = 0
    row_end = 0
    for i_grp in range(new_len):
        row_start = row_end
        row_end += 1 + grp_size if i_grp < reste else grp_size
        new_arr[i_grp] = np.mean(arr[row_start:row_end], axis=0)
    return new_arr


FIG_HARD = go.Figure()
FIG_CONV = go.Figure()

for scale in SCALE_LIST:
    basename=F"./dat/guess_scale/scale_{CASE}_{UMOCK}_"+F"{scale:3.3}"
    print(basename)
    data = pd.read_csv(basename + "_variables.csv").values
    if data.dtype == "O":
        continue
    FIG_HARD.add_trace(
        go.Scatter(
            x=data[:, 0],
            y=data[:, 1],
            name=str(basename),
            mode="markers",
        )
    )
    data = pd.read_csv(basename + "_convergence.csv").values
    newd = coarsen_aray(data, NEW_LEN)
    # FIG_CONV.add_trace(go.Scatter(x=(data[:,0]),y=np.log(data[:,1]),
    #                              mode='markers'))
    s_axis = newd[:, 0]*0. + scale
    #FIG_CONV.add_trace( go.Scatter3d(x=s_axis, y=np.log(newd[:, 0]), z=np.log(newd[:, 1])))
    FIG_CONV.add_trace(
            go.Scatter(
                x=s_axis,
                y=(newd[:, 0]),
                marker={"size": 20,
                        "color":'gray',# newd[:, 1],
                        "opacity": 0.1},
                #alpha=0.8,
                mode='markers',
                )
            )

    #np.savetxt(
    #        'dat/PGF_DATA/SCALE.txt',
    #        newd,
    #        fmt='%.3e',
    #        header = HEADERS['CONV'],
    #        )
FIG_CONV.update_layout(
    title=dict(
        text=F"({UMOCK}_{CASE})",
        font=dict(size=50),
        automargin=True,
        yref='paper',
        ),
    xaxis_title=dict( font=dict(size=30), text="Initial guess scale",),
    yaxis_title=dict( font=dict(size=30), text="log(CPU_TIME)",),
)  
FIG_CONV.show()
#FIG_HARD.show()
