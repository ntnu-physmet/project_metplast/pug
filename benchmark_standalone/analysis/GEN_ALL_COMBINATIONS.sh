#Compile all `./cases/` combined with all `./umocks`

mkdir -p dat

cd cases
for case in *.F; do
 cd ../umocks/
 # There
 casename=$(basename -- "$case")
 casename="${casename%.*}"
 for umock in *.F; do
  # Here
  umatname=$(basename -- "$umock")
  umatname="${umatname%.*}"
  datname="${casename}_${umatname}"
  echo $datname
  # Now
  gfortran -cpp ../cases/${case} ${umock} 
  ./a.out > ../dat/${datname}.log
  cat ../dat/${datname}.log | grep "MAIN" | cut -c7- > ../dat/${datname}_variables.csv
  cat ../dat/${datname}.log | grep "CONVERGENCE" | cut -c14- > ../dat/${datname}_convergence.csv
 done
 cd ../cases
done

cd ..
