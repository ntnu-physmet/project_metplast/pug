# Run tension with different step sizes, then compare

mkdir -p dat

casename="tension"

csv_args=""

cd umocks

#for umockname in *.F; do
for umockname in Holmedal.F; do
 for INCS in  $(cat ../analysis/increment_and_residual_ISIZES.txt) ; do
  for RESS in $(cat ../analysis/increment_and_residual_PSITOLS.txt) ; do
   filename="ten_U${umockname:0:4}_I${INCS}_R${RESS}"
   if [ -f "../dat/irsizes/${filename}.log" ]; then
    echo "(EXISTS) $filename -DINC_SIZE=$INCS -DRES_SIZE=$RESS "
   else
    echo $filename -DINC_SIZE=$INCS -DRES_SIZE=$RESS 
    gfortran \
     -cpp \
     -DLS_MAX=0 -DBEST_FIRST_GUESS \
     -DINC_SIZE=$INCS -DRES_SIZE=$RESS  \
     ../cases/${casename}.F $umockname
    ./a.out > ../dat/${filename}.log
    cat ../dat/irsizes/${filename}.log | grep "MAIN"        | cut -c7-  > ../dat/irsizes/${filename}_variables.csv
    cat ../dat/irsizes/${filename}.log | grep "CONVERGENCE" | cut -c14- > ../dat/irsizes/${filename}_convergence.csv
   fi
   #csv_args=$csv_args" dat/irsizes/${filename}_convergence.csv"
   csv_args=$csv_args" dat/irsizes/${filename}_variables.csv"
  done
 done
done

cd ..
#python analysis/csv_plot.py 0 1 $csv_args
