#!/bin/python3

MSG="""
csv_plot <X-IDX> <Y-IDX> <file1> <file2> ... <fileN>

will plot (x=col1 vs y=col2) of all csvfiles given as args
"""
print(MSG)

import sys

import pandas as pd
import numpy as np

import plotly.graph_objects as go

fig= go.Figure()

print(sys.argv)

X_IDX = int(sys.argv[1])
Y_IDX = int(sys.argv[2])

csvnamelist = (sys.argv[3:])

for csvname in csvnamelist:
    data=pd.read_csv(csvname)
    x_col = data.keys()[X_IDX]
    y_col = data.keys()[Y_IDX]
    fig.add_trace(
            go.Scatter(x=data[x_col], y=data[y_col], mode='markers', name=csvname,)
            )

if len(csvnamelist)>0:
    fig.show()
