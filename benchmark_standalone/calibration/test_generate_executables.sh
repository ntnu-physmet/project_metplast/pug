#!/bin/sh

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

gfortran -cpp -DCALIBRATION -DINC_SIZE=6.e-3 -DLS_MAX=10 cases/tension.F umocks/Holmedal.F      -o test_hol.exe
gfortran -cpp -DCALIBRATION -DINC_SIZE=6.e-3 -DLS_MAX=10 cases/tension.F umocks/ZieglerPrager.F -o test_zie.exe

# Then calibrate with 
#python   calibration/calibrate.py
#      -m test_zie.exe  
#      -r calibration/test_hardening_ref.half.csv 
#      -p calibration/test_params_ziegl.csv

