@mainpage MAIN PAGE (TODO)

The conventions are detailed in CONTRIBUTING.md.
Here we give details here on the formalism, the notations, the vocabulary.


### Rate independent plasticity
The yield function is defined by the difference
\f[ f(\boldsymbol\sigma,\ldots)=\phi(\boldsymbol\sigma,\ldots)  - \sigma_y(\ldots) \f]
where \f$\phi\f$ is the **equivalent stress** and \f$\sigma_R\f$ the **reference stress**.
The choice of additional (internal) variables then defines the **hardening type**

The he evolution is then constrained by the Karush--Kuhn--Tucker (KKT) conditions:
\f[ f\leq0,\quad \mathrm d\lambda\geq0,\quad f\mathrm d\lambda=0. \f]

### Natural notation

The code exploits the usually independence of plasticity with respect to pressure.
Tensors are then projected in their deviatoric subspace: only 5 of the 6 components of stresses and strains are used in most subroutines.
