"""Context file for robustness.
Idea from
https://kenreitz.org/essays/repository-structure-and-python
"""

import logging

import os
import sys

# The insertion below makes the script path-agnostic
sys.path.insert( 0,
        os.path.abspath(
            os.path.join(os.path.dirname(__file__),'../../')
            )
        )

#pylint: disable=import-error, disable=wrong-import-position, disable=unused-import

from client import DEFAULT_NAME
from client import utils_inp
from client import utils_userfile
from client import utils_devfile
from client import paths
from client import parser

########################################################################
## LOGGING
# set logger level to DEBUG (all)
logging.getLogger().setLevel(logging.DEBUG)

# add a file logger just to see
LOG_PATH = os.path.join(os.path.dirname(__file__), 'logging-test.rm-anytime.log')
fh = logging.FileHandler(LOG_PATH)
fh.setLevel(logging.DEBUG)
logging.getLogger().addHandler(fh)


logging.info('COUCOU %s','1')
logging.info('==================================== CONTEXT FILE')

def log_new_test(sss=os.path.basename(__file__).upper()):
    """Something to log when the test file starts"""
    logging.info('')
    logging.info('*********************************************')
    logging.info(sss)
    logging.info('')
