"""Python client unit testing

From this script's path:
>>> pytest -v # scans for appropriate files anyways
"""

import os
import copy
import pytest

from context import utils_userfile as usr

from context import log_new_test
log_new_test(os.path.basename(__file__).upper())

####################################################################################################
# PREPARATION

FILE_NAME = 'sample.yaml'


USR_EX = copy.deepcopy(usr.DEFAULT_USER)

usr.write(USR_EX, file_name=FILE_NAME, open_mode='w',)

####################################################################################################
# WRITING FUNCTIONS

def test_write_on_existing():
    """Try to write on an existing file with default option"""
    with pytest.raises(Exception) as excinfo:
        usr.write(USR_EX, file_name=FILE_NAME)
    assert 'File exists' in str(excinfo.value)



####################################################################################################
# DATA INTEGRITY

def test_check_bad_dict():
    """check a nonsensical user input dict"""
    with pytest.raises(Exception) as excinfo:
        usr.check_dict({'name':'okay input', 'stupid':'input'})
    assert excinfo.errisinstance(KeyError)

def test_check_good_dict():
    """check an okay user input dict"""
    usr.check_dict({'name':'okay input',})

def test_write_wrong_keys():
    """Try to write a dict with wrong keys"""
    with pytest.raises(Exception) as excinfo:
        usr.write({'dada':6}, open_mode='w', file_name='other_'+FILE_NAME)
    assert excinfo.errisinstance(KeyError)

def test_write_empty_header():
    """Try to write a dict with wrong keys"""
    usr.write(
            USR_EX,
            open_mode='w',
            file_name='noheader_'+FILE_NAME,
            header='')
####################################################################################################
# READING FUNCTIONS

def test_read():
    """The reading process goes fine"""
    usr.read(FILE_NAME)

def test_read_as_written():
    """The written data name identical to the read one"""
    usr_dict = usr.read(FILE_NAME)
    assert usr_dict == USR_EX

def test_read_empty_header():
    """Read and check the empty header version"""
    usr_dict = usr.read('noheader_'+FILE_NAME)
    usr.check_dict(usr_dict)

####################################################################################################
# CLEANUP

def test_remove_files():
    """Cleanup dir after writing dummy files"""
    os.remove(FILE_NAME)
    os.remove('noheader_'+FILE_NAME)

####################################################################################################
#

#if __name__ == "__main__":
#    usr.write(USR_EX, file_name=FILE_NAME, open_mode='w',)
#    test_write_on_existing()
#    test_check_bad_dict()
#    test_check_good_dict()
#    test_write_wrong_keys()
#    test_write_empty_header()
#    test_read()
#    test_read_as_written()
#    test_read_empty_header()
#    test_remove_files()
