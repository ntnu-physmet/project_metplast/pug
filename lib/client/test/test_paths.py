"""Python client unit testing

From this script's path:
>>> pytest -v # scans for appropriate files anyways
"""
import os
import pytest
from context import paths as pat
from context import log_new_test
log_new_test(os.path.basename(__file__).upper())

def test_get_main_paths():
    """just print the different directories in use"""
    print(pat.DIR_CUR)
    print(pat.DIR_LIB)
    print(pat.DIR_UMA)

def test_scan_for_dict():
    """Check the dict makes sense"""
    assert 'equivalent_stress' in pat.scan_umat_options(return_type=dict).keys()

def test_scan_for_str():
    """Just print the str contents and check a keyword"""
    print(pat.scan_umat_options(return_type=str))
    assert 'equivalent_stress' in pat.scan_umat_options(return_type=str)

def test_scan_for_wrong():
    """Require a wrong type, here tuple"""
    with pytest.raises(Exception) as excinfo:
        pat.scan_umat_options(return_type=tuple)
    assert excinfo.errisinstance(TypeError)
