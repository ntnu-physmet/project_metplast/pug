C> @file voce.f Voce's hardening rule
C-----------------------------------------------------------------------
C> @author
C>    - Tomáš Mánik: writing core, algorithm, concept
C>    - Baptiste Reyne: refactoring for Doxygen and PUG
C-----------------------------------------------------------------------
C> @details
C>    The hardening parameter yields
C>    \f[
C>      \sigma_y(p) = \sigma_0 + R_\mathrm{sat}
C>      \left(
C>       1-\exp\left(\frac{-p}{p_\mathrm{sat}}\right)
C>      \right)
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------
C> @param[in] params <b>(3)</b> The parameters:
C>    |Symbol   | Name              | Unit  | e.g.  |
C>    |---------|-------------------|-------| ------|
C>    | sigma_0 | initial stress    | MPa   |  20   |
C>    | Rsat    | saturation stress | MPa   | 150   |
C>    | psat    | saturation strain | -     |   0.5 |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] sigma_y the hardening parameter \f$\sigma_y\f$.
C
      subroutine Reference_Stress(params, aps, sigma_y)
      implicit none
      real*8      params(3), aps, sigma_y
      sigma_y = abs(params(1) + params(2)*(1.d0-exp(-aps/params(3))))
      !write(6,*) "sigy=", sigma_y
      end subroutine Reference_Stress


C=======================================================================

C> @author
C>    - Baptiste Reyne: refactoring for Doxygen and PUG

C-----------------------------------------------------------------------
C> @details
C>    The hardening parameter yields
C>    \f[
C>      \frac{\partial\sigma_y}{\partial p}=
C>      \frac{R_\mathrm{sat}}{p_\mathrm{sat}}
C>      \exp\left(\frac{-p}{p_\mathrm{sat}}\right)
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------

C> @param[in] dparams <b>(3)</b> (3) The parameters:

C>    |Symbol                | Name              | Unit  |
C>    |----------------------|-------------------|-------|
C>    | \f$\sigma_0\f$       | initial stress    | MPa   |
C>    | \f$R_\mathrm{sat} \f$| saturation stress | MPa   |
C>    | \f$p_\mathrm{sat} \f$| saturation strain | -     |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] dsy_daps the hardening parameter \f$r\f$.
C>     
      subroutine Reference_Stress_Deriv(dparams, aps, dsy_daps)
      implicit none
      real*8      dparams(3), aps, dsy_daps
      dsy_daps =  abs(dparams(2)*exp(-(aps)/dparams(3))/dparams(3))
      end subroutine Reference_Stress_Deriv
