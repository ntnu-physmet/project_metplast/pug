      include "../equivalent_stress/mises.f"
      include "../hardening_type/kinortional.f"
      include "../elasticity/isotropic.f"
      include "../reference_stress/voce.f"
      include "../common/linear_algebra.f"
      include "../common/utils.f"

      program hexatest
        implicit none

        integer np_RS, np_ES, i, j
        real*8 par_RS(3), par_ES(0), par_HT(4)
        real*8 eqs, grad(5), hess(5,5), C_inv(5,5), jaco(26,26)

        real*8 sig_0(5), X(5), acp

        eqs   = 0.
        grad  = 0.
        hess  = 0.
        C_inv = 0.

        par_HT = (/ 2., 0.001 ,20., 0./)
        par_RS = (/ 50. , 150., 0.5 /)
        np_RS = 3
        !           /--------------------------\  gc    g+    g-
        sig_0 = (/3.d0, 2.d0, 1.d0, -1.d0, 0.d0/) *100.
        X = sig_0 * .00001
        acp = 0.d0

        write(*,*) "***********************************************"

        call Equivalent_Stress(sig_0, PAR_ES, 1, eqs, grad, hess)
        write(*,*) eqs
        write(*,*) grad
        write(*,*) "................................................"
        call EQS_AUGMENTED(sig_0, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    1, eqs, grad)
        write(*,*) eqs
        write(*,*) grad

        write(*,*) "***********************************************"

        call Equivalent_Stress(sig_0, PAR_ES, 4, eqs, grad, hess)
        call print_array(hess, 5, 5)
        write(*,*) "................................................"
        call EQS_AUGMENTED_HESSIAN(sig_0, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    hess)
        call print_array(hess, 5, 5)

        write(*,*) "***********************************************"



      end program hexatest

