


      PROGRAM UMOCK_TESTER
      implicit none

      ! other
      integer n_inc, i
      real*8  d_t

      ! Tensorial directions
      real*8  dir_tension_1(6), dir_shear_1(6),
     +        dir_tension_2(6), dir_shear_2(6),
     +        dir_pressure(6)

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6), statev(11), props(27)
      real*8  ddsdde(6,6)

      stress = (/0.,0.,0.,0.,0.,0./)
      statev = 0.d0
      props=(/
     +  7.e+04, 0.3  ,                 ! (1-2)elast
     +  20.   ,150., 0.5,             ! (3-5)Voce eqn
     +  8.,                           ! (6) a
     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00,
     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00, !7-24
     +  4.  , 200.00, 1.0
c       q   , Xsat, Esta
     +  /)


      d_t      = 1.d0
      n_inc    = 5

      write(*,"(11F7.2)") statev
      write(*,*) "-----------------------------------------------------"

      write(*,*) "====================================================="

      d_strain = (/ 2., 0., 0., 0., 0., 0. /) * 1.d-3
      do i=1,n_inc
        !write(*,"(A3, I2, A)") "⏩ ", i, " (inc starts)"
        CALL Umock(stress, d_strain, d_t, statev, 11, props, 27, ddsdde)
        write(*,"(A3, I2, A)") "⏩ ", i, " (inc ends)"
        write(*,"(A, 6ES10.1)") "stress:", stress
        write(*,"(A, ES15.6)")  "࿇  acp:", statev(1)
        write(*,"(A,5ES10.1)")  "    X1:", statev(2:6)
        write(*,"(A,5ES10.1)")  "    X2:", statev(7:11)
      enddo
      
c     write(*,*) "⏩ =========================================="
c     
c     d_strain = -d_strain
c     !d_strain = (/ 0., 0., 0., -2., 0., 0. /) * 1.d-3
c     do i=1,n_inc
c       !write(*,"(A3, I2, A)") "⏩ ", i, " (inc starts)"
c       CALL Umock(stress, d_strain, d_t, statev, 11, props, 27, ddsdde)
c       write(*,"(A3, I2, A)") "⏩ ", i, " (inc ends)"
c       write(*,"(A, 6ES10.1)") "stress:", stress
c       write(*,"(A, ES15.6)")  "࿇  acp:", statev(1)
c       write(*,"(A,5ES10.1)")  "    X1:", statev(2:6)
c       write(*,"(A,5ES10.1)")  "    X2:", statev(7:11)
c     enddo

      write(*,*) "====================================================="


      END PROGRAM UMOCK_TESTER
       
      include "pool/PUG_holmedal19_tester.f"
