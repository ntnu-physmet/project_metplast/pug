
      include "../equivalent_stress/mises.f"
      include "../elasticity/isotropic.f"
      include "../reference_stress/voce.f"
      include "../common/linear_algebra.f"

      include "../hardening_type/holmedal19.f"

      program holmetest
        implicit none

        integer i, j, k
        real*8 par_EL(2), par_HT(3), par_ES(0), par_RS(3)
        real*8
     +    eqs, grad(5), hess(5,5), C_inv(6,6),
     +    residuals(16), merit_fun, JAC(16,16)
        real*8
     +    acp,   sig(5),   X1(5),   X2(5),
     +  d_acp, d_sig(5), d_X1(5), d_X2(5),
     +  tmp1

        eqs=0.
        grad = 0.
        hess = 0.
        C_inv = 0.

        par_EL = (/ 70.d3, 33.d-2 /)
        par_HT = (/ 3., 200., .5 /)
        par_RS = (/ 20., 150., .5 /)

        acp = .1
        sig = (/-3.d0, 2.d0, 1.d0, -2.d0, 2.d0/)* 1.d0
        call Equivalent_Stress(sig, PAR_ES, 1, eqs, grad, hess)
        write(*,*) 'Mises = ', eqs, sum(grad**2.)

        X1  = +sig * 2
        X2  = -sig * 2

        write(*,*) "***********************************************"

        tmp1=-0.d0

        do i=1,10
          call EQS_AUGMENTED( tmp1*sig, acp, X1, X2, 
     +                        PAR_HT, PAR_ES, 0, PAR_RS, 3,
     +                        1, eqs, grad)
          write(*,*) '........'
          write(*,*) 'EQS_A = ', eqs, sum(grad**2.)
          write(*,*) 'GRAD  = ', grad

          tmp1 = tmp1+1.d0
        enddo

c       write(*,*) "***********************************************"

c       !sig = (/0.d0, 2.d0, 1.d0, 0.d0, 0.d0/)
c       !call Equivalent_Stress(sig, PAR_ES, 4, eqs, grad, hess)
c       !write(*,"(5F7.2)") hess(1,:)
c       !write(*,"(5F7.2)") hess(2,:)
c       !write(*,"(5F7.2)") hess(3,:)
c       !write(*,"(5F7.2)") hess(4,:)
c       !write(*,"(5F7.2)") hess(5,:)

c       write(*,*) "***********************************************"

c       sig = (/0.d0, 2.d0, 1.d0, 0.d0, 0.d0/)

c       call Residual(
c    +    acp,   sig,   X1,   X2, 
c    +    d_acp, d_sig, d_X1, d_X2, 
c    +    2*sig, 1.d6,
c    +    PAR_HT,
c    +    PAR_EL, 2,
c    +    PAR_ES, 0,
c    +    PAR_RS, 3,
c    +    residuals, 
c    +    merit_fun
c    +  )
c       write(*,"(16F7.2)") residuals

c       write(*,*) "***********************************************"

c       call Jacobian(
c    +    acp,   sig,   X1,   X2, 
c    +    d_acp, d_sig, d_X1, d_X2, 
c    +    2*sig, 1.d6,
c    +    PAR_HT,
c    +    PAR_EL, 2,
c    +    PAR_ES, 0,
c    +    PAR_RS, 3,
c    +    JAC
c    +  )
c       write(*,"(16F7.2)") jac(1,:)
c       write(*,"(16F7.2)") jac(2,:)
c       write(*,"(16F7.2)") jac(3,:)
c       write(*,"(16F7.2)") jac(4,:)
c       write(*,"(16F7.2)") jac(5,:)
c       write(*,"(16F7.2)") jac(6,:)
c       write(*,"(16F7.2)") jac(7,:)
c       write(*,"(16F7.2)") jac(8,:)
c       write(*,"(16F7.2)") jac(9,:)
c       write(*,"(16F7.2)") jac(10,:)
c       write(*,"(16F7.2)") jac(11,:)
c       write(*,"(16F7.2)") jac(12,:)
c       write(*,"(16F7.2)") jac(13,:)
c       write(*,"(16F7.2)") jac(14,:)
c       write(*,"(16F7.2)") jac(15,:)
c       write(*,"(16F7.2)") jac(16,:)

c       write(*,*) "***********************************************"

      end program holmetest

