      include "../equivalent_stress/mises.f"
      include "../hardening_type/hexah22.f"
      include "../elasticity/isotropic.f"
      include "../reference_stress/voce.f"
      include "../common/linear_algebra.f"
      include "../common/utils.f"

      program hexatest
        implicit none

        integer np_ES, i, j
        real*8 gh(8,5), q, par_ES(0), par_HT(9)
        real*8 eqs, grad(5), hess(5,5), C_inv(5,5), jaco(26,26)

        real*8
     +    the_res(26),
     +    Ryf, Rep(5), Rha(5), Rpm(10), Rcl(5),
     +    merit_fun     

        real*8 sig_0(5)

        eqs=0.
        grad = 0.
        hess = 0.
        C_inv = 0.

c        gh = (/
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./),
c     +    (/0.,0.,0.,0.,0./)
c     +  /)

        par_HT = (/ 3., 1.,1.,1.,1.,1.,1.,1.,1./)
        q =  3.
        gh = 0.
        !           /--------------------------\  gc    g+    g-
        gh(:,1) = (/1.d0, 0.d0, 0.d0, 0.d0, 0.d0, .7d0, .5d0, .5d0 /)
        gh(:,1) = (/0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0 /)
        sig_0 = (/-3.d0, 2.d0, 1.d0, 0.d0, 0.d0/)

        write(*,*) "***********************************************"

        call Equivalent_Stress(sig_0, PAR_ES, 1, eqs, grad, hess)
        write(*,*) eqs
        call EQS_AUGMENTED(sig_0, GH, q ,PAR_ES,np_ES,1,eqs, grad)
        write(*,*) eqs
        write(*,*) grad

        write(*,*) "***********************************************"

        sig_0 = (/0.d0, 2.d0, 1.d0, 0.d0, 0.d0/)
        call Equivalent_Stress(sig_0, PAR_ES, 4, eqs, grad, hess)
        write(*,"(5F7.2)") hess(1,:)
        write(*,"(5F7.2)") hess(2,:)
        write(*,"(5F7.2)") hess(3,:)
        write(*,"(5F7.2)") hess(4,:)
        write(*,"(5F7.2)") hess(5,:)
        call EQS_AUGMENTED_HESSIAN(sig_0,GH,q,PAR_ES,np_ES,hess)
        write(*,"(5F7.2)") hess(1,:)
        write(*,"(5F7.2)") hess(2,:)
        write(*,"(5F7.2)") hess(3,:)
        write(*,"(5F7.2)") hess(4,:)
        write(*,"(5F7.2)") hess(5,:)

        write(*,*) "***********************************************"

        sig_0 = (/0.d0, 2.d0, 1.d0, 0.d0, 0.d0/)
        gh(:,1) = (/1.d0, 0.d0, 0.d0, 0.d0, 0.d0, .7d0, .5d0, .2d0 /)
        gh(6:8,:) = 1.d0

        call Residual(
     +    sig_0, GH, .005d0, sig_0*0., GH*.1,
     +    par_HT, 1, 100.d0,
     +    eqs, grad, 200.d0,
     +    the_res(1),the_res(2:6),the_res(7:11),
     +    the_res(12:21),the_res(22:26),
c    +    Ryf, Rep, Rha, Rpm, Rcl,
     +    merit_fun
     +  )
        write(*,"(26F7.1)") the_res

        write(*,*) "***********************************************"

        call Jacobian( sig_0, GH, .001d0, GH*.1, 1,
     +    np_ES, par_ES, par_HT, eqs, grad, hess, C_inv, 1.d0, jaco
     +  )

        do i=1,26
          write(*,"(26F7.1)") jaco(i,:) 
        enddo

        write(*,*) "***********************************************"
        write(*,*) reshape(
     +    (/
     +    (/11,12,13/),
     +    (/21,22,23/),
     +    (/31,32,33/)
     +    /)
     +  , (/9/)
     +  )

        write(*,*) "***********************************************"

      end program hexatest

