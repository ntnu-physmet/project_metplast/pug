
      include "../common/linear_algebra.f"
      include "../common/utils.f"

      PROGRAM test
        implicit none

        integer i
        real*8 u(5), A(5,5), A_inv(5,5), MUL(5,5)

        u = (/ 1.,1.,1.,1.,1. /)
        A=0.
        A_inv = 0.

        do i=1,5
        A(i,i) = 1.
        enddo

        A(1,:) = (/ +2., +1., +1., -4., +0. /)
        A(2,:) = (/ +1., +2., +0., +0., -7. /)
        A(3,:) = (/ +1., +0., +1., +0., +2. /)
        A(4,:) = (/ -1., +0., +0., +2., +0. /)
        A(5,:) = (/ -2., +0., +0., +0., +1. /)

        !!write(*,"(5ES12.3)", advance="no") u

        call inverse(A,5, A_inv)

        write(*,*) "BASE MAT"
        write(*,*) "--------"
        call print_array(A, 5, 5)

        write(*,*) "INVERSE "
        write(*,*) "-------"
        call print_array(A_inv, 5, 5)

        call dotprod_mat_mat(A, A_inv, 5, MUL)

        write(*,*) "MULTIPL "
        write(*,*) "-------"
        call print_array(MUL, 5, 5)


      END PROGRAM test
