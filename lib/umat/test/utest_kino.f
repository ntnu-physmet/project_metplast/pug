


      PROGRAM UMOCK_TESTER
      implicit none

      ! other
      integer n_inc, i
      real*8  d_t

      ! Tensorial directions
      real*8  dir_tension_1(6), dir_shear_1(6),
     +        dir_tension_2(6), dir_shear_2(6),
     +        dir_pressure(6)

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6), statev(6), props(28)
      real*8  ddsdde(6,6)

      stress = 1.
      statev = (/ 0.0, 
     +            0.0, 0., 0., 0., 0.
     +          /)
      props=(/
     +  7.e+04, 0.3  ,                 ! (1-2)elast
     +  20.   ,150., 0.5,             ! (3-7)Voce eqn
     +  8.,                           ! (8) a
     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00,
     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00, !9-27
     +  2.  , 0.5, 100., 1.
     +  /)

      d_strain = (/ 1., 0., 0., 0., 0., 0. /) * 1.d-2
      d_t      = 1.d0
      n_inc    = 5

      write(*,*) "====================================================="
      do i=1,n_inc
        CALL Umock(stress,d_strain,d_t,statev,6,props, 28, ddsdde)
      write(*,*) "-----------------------------------------------------"
      enddo

      !write(*,*) ")))", statev

      END PROGRAM UMOCK_TESTER
       
      include "pool/PUG_kinortional_tester.f"
