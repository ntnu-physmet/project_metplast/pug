

      PROGRAM UMOCK_TESTER
      implicit none

      ! other
      integer n_inc, i
      real*8  d_t

      ! Tensorial directions
      real*8  dir_tension_1(6), dir_shear_1(6),
     +        dir_tension_2(6), dir_shear_2(6),
     +        dir_pressure(6)

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6)
      real*8  ddsdde(6,6)

      integer NS, NP
      real*8 statev(200), props(200)
      call GET_SIZES_PS(NP, NS)
      call ASSIGN_PROPS(props, NP)


      stress = (/0.,0.,0.,0.,0.,0./)
      statev = 0.d0
C      props=(/
C     +  7.e+04, 0.3  ,                 ! (1-2)elast
C     +  20.   ,150., 0.5,             ! (3-7)Voce eqn
C     +  8.,                           ! (8) a
C     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00,
C     +  0.0 , 0.0, 1.00,  0.00,  0.00,  1.00,  1.00,  1.00, 1.00, !9-27
C     +  2.  , 100.00, 0.50, 100.0, 0.7, 1.0, 100.0, 0.7, 1.0 
Cc       q   ,   rhoh, chim, rho_c, t_c, gam, rho_b, t_b, b 
C     +  /)


      d_t      = 1.d0
      n_inc    = 5


        CALL print_statev(statev)
      write(*,*) "-----------------------------------------------------"

      write(*,*) "====================================================="
      d_strain = (/ 2., 0., 0., 0., 0., 0. /) * 1.d-4

      do i=1,n_inc
        CALL Umock(stress, d_strain, d_t, statev, NS, props, NP, ddsdde)
        !CALL print_array(reshape(stress, (/1,6/)), 1, 6)
      enddo
        CALL print_statev(statev)
        
c     write(*,*) "-----------------------------------------------------"
c     d_strain = -d_strain

c     do i=1,2*n_inc
c     CALL Umock(stress, d_strain, d_t, statev, 43, props, 33, ddsdde)
c     enddo
c     CALL print_statev(statev)

c     write(*,*) "====================================================="
c     d_strain = (/ 0., 2., 0., 0., 0., 0. /) * 1.d-4
c     
c     do i=1,n_inc
c     CALL Umock(stress, d_strain, d_t, statev, 43, props, 33, ddsdde)
c     enddo
c     CALL print_statev(statev)
c     
c     
c     write(*,*) "-----------------------------------------------------"
c     d_strain = -d_strain
c     
c     do i=1,n_inc
c     CALL Umock(stress, d_strain, d_t, statev, 43, props, 33, ddsdde)
c     enddo
c     CALL print_statev(statev)
      
      
c     write(*,*) "====================================================="
c     d_strain = (/ 2., 0., 0., 0., 0., 0. /) * 1.d-4
c     
c     do i=1,n_inc
c     CALL Umock(stress, d_strain, d_t, statev, 43, props, 33, ddsdde)
c     enddo
c     CALL print_statev(statev)
c     
c     write(*,*) "====================================================="


      END PROGRAM UMOCK_TESTER
       
      include "pool/PUG_hexah_tester.f"
