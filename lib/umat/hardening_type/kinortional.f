
C***********************************************************************
C  Fix the line prefix next line to activate doxygen  ******************
C***********************************************************************

C  @file kinortional.f
C>    One tensorial internal variable (5 scalars)
C>    - backstress

C-----------------------------------------------------------------------
C> @details 
C>    TODO
C>    DDSDDE is not implemented

C> @attention
C>    DDSDDE is not implemented

C-----------------------------------------------------------------------
C> @param[in,out] SVEC
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] DSTRAIN
C>    Total strain increment
C> @param[in] DT
C>    Time increment.

C> @param[in] params <b>(4)</b> 
C>    | symbol   | name                   | unit | default |
C>    |----------|------------------------|------|---------|
C>    | p        | general exponent       |  -   | 4.      |
C>    | epsilon0 | strain scale           |  -   | 0.001   |
C>    | x0       | backstress scale       |  -   | 200.    |
C>    | alpha    | transition coefficient |  -   | 1.0     |

C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.

C> @param[in,out] STATE_VARS  <b>(6)</b> REAL*8 array, dimension (NS)
C>    | Symbol | Name                  | unit |
C>    |--------|-----------------------|------|
C>    | acp    | accum plastic strain  |      |
C>    | X_1    | backstress 1_1        |      |
C>    | X_2    | backstress 1_2        |      |
C>    | X_3    | backstress 1_3        |      |
C>    | X_4    | backstress 1_4        |      |
C>    | X_5    | backstress 1_5        |      |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/d_eps.
C> @param[out] INFO is INTEGER
C>    INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    INFO = -1 means unsuccessful finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

C> @todo deal with info, test umat

      subroutine Return_Map(sig, d_eps, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      implicit none  

      ! (in/out)
      integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      real*8   sig(6), d_eps(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      integer  i, i_max, j, j_max, k, l
      
      real*8 ! State variables
     +     sig_dev(5),    acp,    X(5),
     +   d_sig_dev(5),  d_acp,  d_X(5), ! increments
     +  dd_sig_dev(5), dd_acp, dd_X(5)  ! iterates
      real*8
     +  sig_trial(5),
     +  d_eps_p(5), d_sig(6)            
      real*8  q,  !params
     +  sig_ref, eqs, grad(5), hess(5,5), dRdp ! yld fun
      real*8  !other
     +  dim_sig,
     +  psi_0, psi, psi_tol, alpha, eta, beta, ! algo
     +  residuals(11),
     +  C_inv(6,6), jac(11,11), tmp11(11)


      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      psi_tol = 1.d-20
      eta = 1.d-1
      beta = 1.d-4
      i_max = 10
      j_max = 0

      !=================================================================
      ! UNPACK STATEVARS/PARAMS
      !-----------------------------------------------------------------

      acp = state_vars(1)
      X = state_vars(2:6)

      if (sum(X*X).LT.1d-10) then
        X = 1.d-6
      endif


      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      ! Elastic guess
      call dStrain_to_dStress(d_eps, par_EL, d_sig)
      sig_trial = sig(2:6) + d_sig(2:6)

      d_acp = 0.d0
      d_eps_p = 0.d0
      d_X = 0.d0

      ! Evaluate the yield function  f = eqs - sig_ref for trial stress
      call Reference_Stress(par_RS, acp, sig_ref)
      call EQS_AUGMENTED(sig_trial, acp, X, 
     +  params, PAR_ES, np_ES, PAR_RS, np_RS, 
     +  1, eqs, grad)

      ! Tangent operators
      call Tangent_Compliance(par_EL, C_inv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)

c     write(*,*) "END", eqs, "<?<", sig_ref

      IF (eqs.LE.sig_ref) THEN

        alpha = 0.
        sig_dev = sig_trial

        ! State_vars are unchanged
        ! Trial stress was good

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

        ! Initialize newton iteration
        sig_dev = X + (sig_trial-X)*sig_ref/eqs
        d_sig_dev = sig_dev - sig(2:6)
  
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_acp   = sum(d_eps_p*(sig_dev-X))/sig_ref
        acp     = acp  + d_acp
        d_X     = 0.d0!(sig_dev-X)*d_acp
        X     = X+d_X

        ! Compute residual
        call Reference_Stress(par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)
        call EQS_AUGMENTED(sig_dev, acp, X, 
     +    params, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    1, eqs, grad)
        call Residual(sig_dev, X, 
     +                d_X, d_acp, d_eps_p,
     +                params, eqs, grad, sig_ref, dim_sig,
     +                residuals(1),residuals(2:6),residuals(7:11),
     +                psi_0)


        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        

        i = 0
        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))
        !do while (i .LE. i_max-1)
          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory
          i = i+1

c         write(*,*) "NEWTON iteration", i

          ! ............................................................
          ! NUMERICAL ! Inversion 

          call Reference_Stress(par_RS, acp, sig_ref)
          call Reference_Stress_Deriv(par_RS, acp, dRdp)
          call Jacobian(
     +      sig_dev, acp, X,
     +      d_acp,
     +      np_RS, np_ES,
     +      params, PAR_ES, PAR_RS,
     +      dRdp, C_inv(2:6,2:6),
     +      jac
     +    )

          call linsolve(jac, tmp11, -residuals, 11)

          ! Update increments
          dd_acp     = tmp11(1)
          dd_sig_dev = tmp11(2:6)
          dd_X       = tmp11(7:11)

          ! ............................................................
          ! Residual (U+DU)
          call Reference_Stress(
     +      par_RS, acp+dd_acp, sig_ref)
          call dStress_to_dStrain_dev(sig_trial-sig_dev-dd_sig_dev,
     +      par_EL, d_eps_p)
          call EQS_AUGMENTED(sig_dev+dd_sig_dev, acp+dd_acp, X+dd_X,
     +      params, PAR_ES, np_ES, PAR_RS, np_RS, 
     +      1, eqs, grad)
          call Residual(sig_dev+dd_sig_dev, X+dd_X, 
     +                  d_X+dd_X, d_acp+dd_acp, d_eps_p,
     +                  params, eqs, grad, sig_ref, dim_sig,
     +                  residuals(1),residuals(2:6),residuals(7:11),
     +                  psi)

          ! ............................................................
          ! Line search 
          psi_0 = psi
          j = 0
          alpha = 1.d0
          do while ((psi.GT.(1.-2.*beta*alpha)*psi_0) 
     +             .and. (j .LT. j_max))
            ! entering this loop means alpha=1. is not satisfactory.
            ! Let us adjust it and test over and over
            j = j+1
 
            alpha = max(eta*alpha, psi_0/(psi- psi_0*(1-2*alpha)))
 
            !! Compute residual U+alpha*DU
            call Reference_Stress(
     +        par_RS, acp+alpha*dd_acp, sig_ref)
            call dStress_to_dStrain_dev(
     +        sig_trial-sig_dev-alpha*dd_sig_dev,
     +        par_EL, d_eps_p)
            call EQS_AUGMENTED(
     +        sig_dev+alpha*dd_sig_dev, acp+alpha*dd_acp, X+alpha*dd_X,
     +        params, PAR_ES, np_ES, PAR_RS, np_RS, 
     +        1, eqs, grad)
            call Residual(sig_dev+alpha*dd_sig_dev, X+alpha*dd_X, 
     +                    d_X+alpha*dd_X, d_acp+alpha*dd_acp, d_eps_p,
     +                    params, eqs, grad, sig_ref, dim_sig,
     +                    residuals(1),residuals(2:6),residuals(7:11),
     +                    psi)
          enddo

          ! ............................................................
          ! Final merit update

          psi_0 = psi
          write(*,*) "PSI=", psi, j ,"LS iter", alpha

          ! ------------------------------------------------------------
          ! Final state variables updates
          ! ------------------------------------------------------------

          d_sig_dev = d_sig_dev + alpha * dd_sig_dev
          d_acp     = d_acp     + alpha * dd_acp
          d_X       = d_X       + alpha * dd_X  

          sig_dev = sig_dev + alpha * dd_sig_dev
          acp     = acp     + alpha * dd_acp
          X       = X       + alpha * dd_X  

        ! --------------------------------------------------------------
        ! End of NEWTON iteration
        ! --------------------------------------------------------------         NEWTON END
        enddo

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------

        !TODO
        
      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------

      sig(1) = sig(1)+d_sig(1)
      sig(2:6)        = sig_dev
      state_vars(1)   = acp
      state_vars(2:6) = X

      write(*,*) "END", state_vars
      write(*,*) "END", eqs, "<?<", sig_ref

      ! ================================================================
      ! Manual WARNINGS for NANs
      ! ----------------------------------------------------------------
      if (sum(sig).NE.sum(sig))       write(*,*) "/!\ sig"
      if (sum(X).NE.sum(X))           write(*,*) "/!\ X"
      if (sum(algmod).NE.sum(algmod)) write(*,*) "/!\ algmod"
      if (acp.NE.acp)                 write(*,*) "/!\ acp"


      RETURN

      end subroutine Return_Map





C#######################################################################



C-----------------------------------------------------------------------
C> @details Residual


      subroutine Residual(
     +  sig_dev, X, 
     +  d_X, d_acp, d_eps_p,
     +  par_HT, eqs, eqs_grad, sig_ref, dim_sig,
     +  Ryf, Rep, Rx,
     +  merit_fun
     +)
      ! Here we compute the total residual for each contribution except
      ! Rhi which is not useful since it amounts to 0-0 = 0

        implicit none
        ! IN/OUT
        real*8
     +    sig_dev(5), X(5), 
     +    d_X(5), d_acp, d_eps_p(5),
     +    par_HT(4),
     +    eqs, eqs_grad(5),
     +    sig_ref, dim_sig,
     +    Ryf, Rep(5), Rx(5),
     +    merit_fun                 
        ! Internal
        real*8 s_hat(5)


        ! ==============================================================
        ! Unpack
        if (sum(sig_dev*sig_dev).LT.1E-20) then
          s_hat = 0.
        else
          s_hat = sig_dev/sum(sig_dev*sig_dev)**.5d0
        endif


        ! ==============================================================
        ! Compute residuals individually
        Ryf = eqs - sig_ref
        Rep = d_acp*eqs_grad - d_eps_p
        Rx  = (s_hat*par_HT(2)-X)*d_acp/par_HT(3) - d_X

        


        ! ==============================================================
        ! Compute The merit function with normalized residuals
        merit_fun = 
     +    (Ryf/dim_sig)**2.d0
     +    + sum(Rep*Rep)
     +    + sum(Rx*Rx)/dim_sig**2.d0

        if (Ryf.NE.Ryf)            write(*,*) "/!\ RESI_yf NaN"
        if (sum(Rep).NE.sum(Rep))  write(*,*) "/!\ RESI_ep NaN"
        if (sum(Rx).NE.sum(Rx))    write(*,*) "/!\ RESI_x  NaN"
        RETURN
      end subroutine Residual

C> @details Jacobian
      subroutine Jacobian(
     +  sig, acp, X,
     +  d_acp,
     +  np_RS, np_ES,
     +  par_HT, PAR_ES, PAR_RS,
     +  h, C_inv,
     +  jac
     +)                  
        implicit none
        ! IN/OUT
        integer np_RS, np_ES
        real*8
     +    sig(5), acp, X(5),
     +    d_acp,
     +    par_HT(4), PAR_ES(np_ES), PAR_RS(np_RS),
     +    h, C_inv(5,5),
     +    jac(11,11)

        ! Internal
        integer i,j
        real*8
     +    s_hat(5), s_norm,
     +    dummy1, grad(5), deriv_X(5), deriv_acp, hess(5,5), tmp


        ! Unpack =======================================================


        ! Prepings =====================================================
        call EQS_AUGMENTED(sig, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    1, dummy1, grad)
        call dEQS_dACP(sig, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    deriv_acp)
        call dEQS_dX(sig, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    deriv_X)
        call EQS_AUGMENTED_HESSIAN(
     +    sig, acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    hess
     +  )
        s_norm = sum(sig*sig)**.5d0
        s_hat = sig/s_norm

        ! INITIALIZE
        jac = 0.

        ! Yield function ===============================================
        jac(1,1) = -h + deriv_acp
        jac(1,2:6) = grad
        jac(1,7:11) = deriv_X

        ! Elastic residual =============================================
        jac(2:6,1)    = grad
        jac(2:6,2:6)  = C_inv + hess*d_acp
        jac(2:6,7:11) = 0.

        ! Backstress residual ==========================================
        jac(7:11,1)    = (s_hat * par_HT(2)-X)/par_HT(3)
        tmp = par_HT(2) * d_acp / s_norm / par_HT(3)
        do i=1,5
          jac(6+i,1+i) = tmp
          do j=1,5
            jac(6+i,1+j)  = jac(6+i,1+j) + tmp * s_hat(i)*s_hat(j)
          enddo
        enddo
        jac(7:11,7:11) = 0.
        tmp = d_acp / par_HT(3) - 1.d0
        do i=1,5
          jac(6+i,6+i) = tmp
        enddo

        RETURN
      end subroutine Jacobian



C-----------------------------------------------------------------------
      subroutine EQS_AUGMENTED(
     +  sig, acp, X, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  mode, eqs, grad
     +  )
        implicit none
        integer mode, np_RS, np_ES
        real*8
     +    acp, X(5), sig(5),
     +    par_ES(np_ES),
     +    PAR_HT(4), PAR_RS(np_RS), 
     +    eqs, grad(5)
        real*8
     +    alpha, p, s_r, 
     +    tmp55(5,5), tmp0, 
     +    tilde_5(5), tilde_0, f, psi_of_X, psi_tilde, xi,
     +    grad_X(5),dpsi_dsig(5), dxi_dsig(5)

        !...............................................................
        ! Assign 
        p     = PAR_HT(1)
        alpha = PAR_HT(4)

        !...............................................................
        ! Compute 
        call linear_transfo(
     +    acp, X, sig,
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    tilde_5)
        call Equivalent_Stress(
     +      tilde_5, par_ES, 1, psi_tilde, grad, tmp55)
        call Equivalent_Stress(
     +      sig, par_ES, 2, tmp0, grad, tmp55)

        if (sum(grad).ne.sum(grad)) grad = 0.d0 ! check NaN

        !...............................................................
        call linear_transfo(
     +    acp, X, X,
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    tilde_5)
        call Equivalent_Stress(
     +      tilde_5, par_ES, 1, f, grad_X, tmp55)

        call Reference_Stress(par_RS, acp, s_r)

        f = 1 - abs(s_r - 2*f)/s_r

        !...............................................................

        xi = sum((2*X - sig)*grad_X)
        if (xi.LT.0.d0) xi=0.d0

        !...............................................................
        ! Conclude
        eqs = ( psi_tilde**p + f * xi**p )**(1.d0/p)

        if (EQS.NE.EQS)       write(*,*) "/!\ EQS NaN"
      !=================================================================
        IF (mode.EQ.0) RETURN!==========================================
      !=================================================================

        call linear_transfo(!
     +    acp, X, grad,
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    dpsi_dsig)

        call linear_transfo(
     +    acp, X, grad_X,
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +    dxi_dsig)

        grad=
     +    (psi_tilde/eqs)**(p-1.) * dpsi_dsig
     +    + f*(xi/eqs)**(p-1.) * dxi_dsig


        if (sum(grad).NE.sum(grad)) write(*,*) "/!\ grad"
        !if (sum(grad).NE.sum(grad)) write(*,*) eqs, p, f, grad
        !if (sum(grad).NE.sum(grad)) write(*,*) dpsi_dsig
        !if (sum(grad).NE.sum(grad)) write(*,*) dxi_dsig

      !=================================================================
        IF (mode.EQ.1) RETURN!==========================================
      !=================================================================

        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"
        write(*,*) "/!\ WARNIIIIIIIIIIIIIIIING, hessian requested"
        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"

        RETURN

      end subroutine EQS_AUGMENTED
      
      subroutine EQS_AUGMENTED_HESSIAN(
     +  sig, acp, X, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  hess
     +)
        implicit none
        ! i/o
        integer np_RS,np_ES
        real*8
     +    sig(5), acp, X(5), 
     +    PAR_HT(4), PAR_ES(np_ES), PAR_RS(np_RS), 
     +    hess(5,5)
        ! internal
        integer i, j
        real*8 d_size, d_sig(5)
        real*8 grad(5), dummy1
        real*8 gplus(5), gminus(5)


        ! We shall compute the gradient derivative numerically, with a
        ! simple mid-point rule
        ! This means 5*5 * 2 calls to the evaluation function

        !---------------------------------------------------------------
        ! Initialize
        d_size = 1.d-6

        !---------------------------------------------------------------
        ! Compute each derivative individually, numerically
        do j=1,5
          d_sig = 0.d0
          d_sig(j) = d_size
          call EQS_AUGMENTED(sig+d_sig, acp, X, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +      1, dummy1, gplus)
          call EQS_AUGMENTED(sig-d_sig, acp, X, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +        1, dummy1, gminus)
          hess(:,j) = (gplus - gminus)/2/d_size
        enddo

        !call print_array(reshape(grad, (/1,5/)), 1, 5)

        RETURN

      end subroutine EQS_AUGMENTED_HESSIAN

C-----------------------------------------------------------------------      

      subroutine dEQS_dACP(
     +  sig, acp, X, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  deriv)
        implicit none
        integer np_ES, np_RS
        real*8
     +    sig(5), acp, X(5),
     +    PAR_HT(4),PAR_ES(np_ES),PAR_RS(np_RS),
     +    deriv
        real*8 eqs, d_acp, grad(5)

        ! Initialize
        d_acp = 1.d-12

        call EQS_AUGMENTED(sig, acp+d_acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    0, eqs, grad)

        deriv = eqs

        call EQS_AUGMENTED(sig, acp-d_acp, X, 
     +    PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +    0, eqs, grad)

        deriv = (deriv - eqs)/2.d0/d_acp

        RETURN
      end subroutine dEQS_dACP

C-----------------------------------------------------------------------      

      subroutine dEQS_dX(
     +  sig, acp, X, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  deriv)
        implicit none
        integer np_ES, np_RS
        real*8
     +    sig(5), acp, X(5),
     +    PAR_HT(4),PAR_ES(np_ES),PAR_RS(np_RS),
     +    deriv(5)

        ! internal
        integer i, j
        real*8 d_size, d_X(5)
        real*8 grad(5), eqs


        ! We shall compute the gradient derivative numerically, with a
        ! simple mid-point rule
        ! This means 5*5 * 2 calls to the evaluation function

        !---------------------------------------------------------------
        ! Initialize
        d_size = 1.d-6

        !---------------------------------------------------------------
        ! Compute each derivative individually, numerically
        do j=1,5
          d_X = 0.d0
          d_X(j) = d_size
          call EQS_AUGMENTED(sig, acp, X+d_X, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +      0, eqs, grad)
          deriv(j) = eqs
          call EQS_AUGMENTED(sig, acp, X-d_X, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS, 
     +        0, eqs, grad)
          deriv(j) = (deriv(j) - eqs)/2.d0/d_size
        enddo

        !call print_array(reshape(grad, (/1,5/)), 1, 5)

        RETURN

      end subroutine dEQS_dX
C-----------------------------------------------------------------------      
C-----------------------------------------------------------------------      
C-----------------------------------------------------------------------      
C-----------------------------------------------------------------------      
C-----------------------------------------------------------------------      

      subroutine linear_transfo(
     +  acp, X, rhs,
     +  PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +  res
     +)
        implicit none
        integer np_ES, np_rs
        real*8 
     +    rhs(5), PAR_RS(np_rs), PAR_HT(4), PAR_ES(np_ES),
     +    res(5), acp, X(5),
     +    x_r, s_r, alpha, 
     +    tmp55(5,5)

        ! Assign 
        alpha = PAR_HT(4)
        if (sum(X*X).LT.1.E-25) then
          res = rhs
        else
          ! Fix Sr and Xr
          call Reference_Stress(par_RS, acp, s_r)
          call Equivalent_Stress(
     +      X, par_ES, 0, x_r, res, tmp55)
          
          res = rhs * s_r / (s_r + alpha * x_r)
     +      + X * sum(rhs*X) / sum(X*X)
     +        * (1.d0 - alpha) * s_r * x_r
     +        / (s_r + alpha*x_r) / (s_r + x_r)
        endif

        if (sum(res).NE.sum(res)) write(*,*) "/!\ LIN_TRA see rhs"
        
        RETURN
      end subroutine linear_transfo

c     subroutine print_real(num)
c       implicit none
c       real*8 num
c       if  (num.EQ.0.d0) then
c         write(*,"(A12)", advance="no") "-"
c       else
c         write(*,"(ES12.3)", advance="no") num
c       endif
c     end subroutine print_real

c     subroutine print_array(arr, a, b)
c       implicit none
c       integer a, b, i, j
c       real*8 arr(a,b)

c       do i=1,a
c         do j=1, b
c           call print_real(arr(i,j))
c         enddo
c         write(*,*)
c       enddo
c     end subroutine print_array

