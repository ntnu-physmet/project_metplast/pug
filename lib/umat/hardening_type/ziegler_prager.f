C> @file ziegler_prager.f 
C>    Two internal variables named
C>    - accumulated plastic strain denoted \f$p\f$.
C>    - smth that conjugates with X

C> @attention The usual Prager-Ziegler model is only kinematic, not
C>    isotropic.
C>    This combination can bring unwanted behaviors: we advise to use
C>    this model without isotropic hardening.
C>    

C-----------------------------------------------------------------------
C> @details 
C>    This is the return mapping predictor-corrector algorithm for 
C>    solving the closest-point projection in an elastic-plastic 
C>    problem. It is based on a Newton-Raphson method with 
C>    line-search.
C>    The algorithm is based on the paper by Scherzinger, W. M.
C>    @cite scherzinger2017.
C>    This particular implementation is for isotropic elasticity, 
C>    isotropic Voce hardening and Yld2004-18p yield surface.
C>    The implementation is detailed by Manik et al. @cite manik2021.

C-----------------------------------------------------------------------
C> @param[in,out] SVEC
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] DSTRAIN
C>    Total strain increment
C> @param[in] DT
C>    Time increment.

C> @param[in] params <b>(1)</b> 
C>    | symbol | name          | unit | default |
C>    |--------|-------------- |------|---------|
C>    | eps_x  | scale of Xdot | -    | 2000.   |

C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.

C> @param[in,out] STATE_VARS  <b>(6)</b> REAL*8 array, dimension (NS)
C>    | Symbol | Name                       |
C>    |--------|----------------------------|
C>    | acp    | accum plastic strain       |
C>    | X1     | back stress   1            |
C>    | X2     | back stress   2            |
C>    | X3     | back stress   3            |
C>    | X4     | back stress   4            |
C>    | X5     | back stress   5            |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/dstrain.
C> @param[out] INFO is INTEGER
C>    INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    INFO = -1 means unsuccesfull finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

C> @todo deal with info, test umat

      subroutine Return_Map(svec, dstrain, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      implicit none  

      ! (in/out)
      integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      real*8   svec(6), dstrain(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      integer  i, i_max, j, j_max, k, l, p
      real*8   sig_trial(5), sig_dev(5), acp, X(5), d_lam,  ! state vars
     +         d_sig_dev(5), d_acp, d_X(5), dd_lam,         ! increments
     +         d_eps_p(5), d_sig(6),                        ! others
     +         sig_ref, eqs, eqs_grad(5), eqs_hess(5,5), h, !yld fun
     +         eps_x,                                       ! backstress model
     +         res_f, res_e(5), res_p, res_x(5), dim_sig,
     +         U_inv(5,5), 
     +         choldiag(5), cholres(5), cholres_other(5),
     +         psi_0, psi, psi_tol, alpha, eta, beta, ! algo
     +         tmp1, tmp5(5), tmp12(12), residuals(12),
     +         Cinv(6,6), jac(12,12), Lstar(5,5)

      LOGICAL NUMERICAL_INTEGRATION


      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      psi_tol = 1.d-20
      eta = 1.d-1
      beta = 1.d-4
      i_max = 1000
      j_max = 10
      NUMERICAL_INTEGRATION = .FALSE.

      !=================================================================
      ! SPLIT ARRAYS
      !-----------------------------------------------------------------

      acp   = state_vars(1)
      X     = state_vars(2:6)

      eps_x = params(1)
      d_eps_p = 0.d0!dstrain(2:6)

      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      call dStrain_to_dStress(dstrain, par_EL, d_sig)
      sig_trial = svec(2:6) + d_sig(2:6)

      ! Evaluate the yield function  f = eqs - sig_ref
      call Reference_Stress(par_RS, acp, sig_ref)
      call Equivalent_Stress(sig_trial-X, par_ES, 1,
     +                       eqs, eqs_grad, eqs_hess)

      ! Tangent operators
      call Tangent_Compliance(par_EL, Cinv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)

c     write(6,*) "# TEST: ELASTIC, eps_x=", eps_x
c     write(*,*) ">  compare", eqs, " to ", sig_ref, "at", acp

      IF (eqs.LE.sig_ref) THEN

c       write(6,*) "# ELASTIC STEP"

        alpha = 0.
        sig_dev = sig_trial

        ! State_vars are unchanged
        ! Trial stress was good

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

c       write(6,*) "# PLASTIC STEP"

        ! Initialize newton iteration
        sig_dev = X + (sig_trial-X)*sig_ref/eqs ! FIRSTGUESS
        !sig_dev = sig_trial

        d_sig_dev = sig_dev - svec(2:6)

        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_lam   = sum(d_eps_p*(sig_dev-X))/sig_ref
        acp     = acp  + d_lam

        d_X     = (sig_dev-X)/eps_x*d_lam
        dd_lam  = 0.d0
        d_acp   = d_lam
  
        ! Compute residual
        call Reference_Stress(par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(sig_trial-sig_dev-d_sig_dev,
     +                              par_EL, d_eps_p)
        call Equivalent_Stress(sig_dev+d_sig_dev-X-d_X, 
     +                         par_ES, 3,
     +                         eqs, eqs_grad, eqs_hess)
        call Residual( sig_dev,X+d_X,d_lam,        ! main variables
     +                 d_eps_p,d_acp,d_X,          ! increments
     +                 eqs,eqs_grad,sig_ref,       ! other vars
     +                 eps_x, dim_sig,             ! params
     +                 state_vars(2:6),            ! X at start
     +                 res_f, res_e, res_p, res_x, ! resisuals
     +                 psi_0)                      ! merit function


        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        
                                                                                ! NEWTON START

c       write(*,*) i, "go  Newt-loop with psi0=", psi_0
        !psi_0 = max(psi_0, psi_tol*1.001)
        !write(*,*) i, "go  Newt-loop with psi0=", psi_0

        i = 0
        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))
          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory

          ! ............................................................
          ! Initializations
          i = i+1
  
          ! Get Uinv and prepare choleski things
          U_inv = 0.d0
          do k=1,5
            U_inv(k,k) = 1 + d_lam/eps_x
            do l=k,5
              !U_inv(k,l)= U_inv(k,l)
              do p=1,5
                U_inv(k,l)= U_inv(k,l)
     +                  + algmod(1+k,1+p) *
     +                    eqs_hess(min(l,p),max(l,p)) * d_lam
              enddo
            enddo
          enddo
          !write(*,*) U_inv
          ! (lower triangle of Uinv will be modified but the upper
          ! triangle incl. the diagonal will not)
          call chol_decomp(U_inv, 5, choldiag)


          call Reference_Stress(par_RS, acp, sig_ref)
          call Reference_Stress_Deriv(par_RS, acp, h)

          ! ............................................................
          ! Inversion 
          ! NUMERICAL
          if (NUMERICAL_INTEGRATION) then
            call Jacobian(sig_dev,X,d_lam,            ! main variables
     +                      h, eqs_grad, eqs_hess,
     +                      eps_x, dim_sig,
     +                      Cinv(2:6,2:6),
     +                      jac)
            residuals(1) = res_f
            residuals(2:6) = res_e
            residuals(7) = res_p
            residuals(8:12) = res_x
            call linsolve(jac, tmp12, -residuals, 12)

            d_sig_dev = tmp12(1:5)
            d_acp = tmp12(6)
            d_X = tmp12(7:11)
            dd_lam = tmp12(12)


          ! SEMI-ANALYTICAL
          else
            ! Multiplier
            call chol_solve(U_inv, 5, eqs_grad, choldiag, cholres)
            call dStrain_to_dStress_dev(res_e, par_EL, tmp5)
            dd_lam = (
     +        res_f
     +        - h*res_p
     +        - sum(cholres*(tmp5+res_x))
     +      )




            call dStrain_to_dStress_dev(eqs_grad, par_EL, tmp5)
            dd_lam = dd_lam/(
     +        h
     +        + sum(cholres*((sig_dev-X)/eps_x+tmp5))
     +      )

            ! Accumulated plastic strain 
            d_acp = res_p + dd_lam

            ! Stress MINUS Backstress stored in d_X temporarily
            call chol_solve(U_inv, 5,
     +                      (tmp5+(sig_dev-X)/eps_x)*dd_lam,
     +                      choldiag, cholres
     +                      )
            d_X = -cholres
            call dStrain_to_dStress_dev(res_e, par_EL, tmp5)
            call chol_solve(U_inv, 5,
     +                      tmp5+res_x,
     +                      choldiag, 
     +                      cholres)
            d_X  = d_X - cholres

            ! Stress
            ! Careful since eqs_hess is stored on the sup triangle
            d_sig_dev=0.
            do k=1,5
              do l=k,5
                d_sig_dev(k) = d_sig_dev(k)
     +                       + eqs_hess(min(k,l),max(k,l))*d_X(l)*d_lam
              enddo
            enddo
            d_sig_dev = res_e + eqs_grad*dd_lam
            call dStrain_to_dStress_dev(-d_sig_dev, par_EL, d_sig_dev)
          endif
          
          d_X = d_sig_dev - d_X



          ! ............................................................
          ! Residual (U+DU)
          call dStress_to_dStrain_dev(sig_trial-sig_dev-d_sig_dev,
     +                                par_EL, d_eps_p)
          call Equivalent_Stress(sig_dev+d_sig_dev-X-d_X, 
     +                           par_ES, 3,
     +                           eqs, eqs_grad, eqs_hess)
          call Reference_Stress(par_RS, acp+d_acp, sig_ref)
          call Residual(sig_dev+d_sig_dev,        ! main variables
     +                  X+d_X,                    ! ...
     +                  d_lam+dd_lam,             ! ... + a+dx
     +                  d_eps_p,d_acp,d_X,              ! increments
     +                  eqs,eqs_grad, sig_ref,          ! other vars
     +                  eps_x, dim_sig,                 ! params
     +                 state_vars(2:6),            ! X at start
     +                  res_f, res_e, res_p, res_x,     ! resisuals
     +                  psi)  

          ! ............................................................
          ! Line search 
          !psi_0 = psi
          j = 0
          alpha=1.d0
          do while ((psi.GT.(1.-2.*beta*alpha)*psi_0) 
     +             .and. (j .LE. j_max))
            ! entering this loop means alpha=1. is not satisfactory.
            ! Let us adjust it and test over and over
            j = j+1

            alpha = max(eta*alpha, psi_0/(psi- psi_0*(1-2*alpha)))

            !! Compute residual U+alpha*DU
            call dStress_to_dStrain_dev(
     +                        sig_trial-sig_dev-alpha*d_sig_dev,
     +                        par_EL, d_eps_p)
            call Equivalent_Stress(sig_dev+alpha*d_sig_dev-X-alpha*d_X, 
     +                             par_ES, 3,
     +                             eqs, eqs_grad, eqs_hess)
            call Reference_Stress(par_RS, acp+alpha*d_acp, sig_ref)
            call Residual(sig_dev+alpha*d_sig_dev,        ! main variables
     +                    X      +alpha*d_X,              ! ...
     +                    d_lam  +alpha*dd_lam,           ! ... + a+dx
     +                    d_eps_p,alpha*d_acp,alpha*d_X,  ! increments
     +                    eqs,eqs_grad, sig_ref,          ! other vars
     +                    eps_x, dim_sig,                 ! params
     +                 state_vars(2:6),            ! X at start
     +                    res_f, res_e, res_p, res_x,     ! resisuals
     +                    psi)

          enddo
          !alpha = 1.

          ! ............................................................
          ! Final merit update

          psi_0 = psi
c         write(*,*) i,"in Newt-loop with psi0=",psi_0,"alpha=",alpha

          ! ------------------------------------------------------------
          ! Final state variables updates
          ! ------------------------------------------------------------

          sig_dev = sig_dev + alpha * d_sig_dev
          d_lam   = d_lam   + alpha * dd_lam
          acp     = acp     + alpha * d_acp
          X       = X       + alpha * d_X

          d_sig(1) = alpha * d_sig(1)
          d_sig(2:6) = alpha * d_sig_dev

        ! --------------------------------------------------------------
        ! End of NEWTON iteration
        ! --------------------------------------------------------------         NEWTON END
        enddo

c       write(*,*) "> END:  compare", eqs, " to ", sig_ref, "at", acp

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------
        
        ! For now algmod = C. FIX IT!
        ! U_inv = C_inv + DLam/(1+DLam/epsx N_,sig)

        call Reference_Stress_Deriv(par_RS, acp, h)

        ! Store L*inv in Uinv
        U_inv  = Cinv(2:6,2:6) + eqs_hess*d_lam/(1.+d_lam/eps_x)
        call chol_decomp(U_inv, 5, choldiag)

        ! Comput L*:N, L*:Xsi, L*
        call chol_solve(U_inv, 5, eqs_grad, choldiag, cholres)
        call chol_solve(U_inv, 5, sig_dev - X, choldiag, cholres_other)
        call chol_inverse(U_inv, 5, choldiag, Lstar)

        cholres_other = cholres_other
     +                * (d_lam/eps_x) / (1.d0 + d_lam/eps_x)

        ! Compute dednominator
        tmp1 = h*(1.d0 + d_lam/eps_x)
     +       + sum(cholres*eqs_grad)
     +       - sum(cholres_other*eqs_grad)
     +       + sum((sig_dev-X)*eqs_grad)/eps_x

        do k=1,5
          do l=1,5
            algmod(1+k,1+l) = Lstar(k,l)
     +        + (cholres_other(k)-cholres(k)) * cholres(l) / tmp1
          enddo
        enddo

      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------

      svec(1) = svec(1)+d_sig(1)
      svec(2:6) = sig_dev

      state_vars(1)   = acp
      state_vars(2:6) = X 

      !write(*,*) "---"
      if (sum(svec).NE.sum(svec))             write(*,*) "/!\ svec"
      if (sum(algmod).NE.sum(algmod))         write(*,*) "/!\ algmod"
      if (acp.NE.acp)       write(*,*) "/!\ acp"
      if (d_lam.NE.d_lam)   write(*,*) "/!\ d_lam"
      if (sum(X).NE.sum(X)) write(*,*) "/!\ X"

      RETURN

      end subroutine Return_Map





C#######################################################################



C-----------------------------------------------------------------------
C> @details Residual


      subroutine Residual(sig_dev,X,d_lam,            ! main variables
     +                    d_eps_p,d_acp,d_X,          ! increments
     +                    eqs,eqs_grad,sig_ref,       ! other vars
     +                    eps_x, dim_sig, X_0,        ! params
     +                    res_f, res_e, res_p, res_x, ! resisuals
     +                    merit_fun)                  ! psi
        implicit none

        real*8 sig_dev(5), X(5), d_lam,
     +         d_eps_p(5),d_acp,d_X(5),         
     +         eqs,eqs_grad(5),sig_ref,      
     +         eps_x, dim_sig, X_0(5),
     +         res_f, res_e(5), res_p, res_x(5),
     +         merit_fun                 

        ! Compute residuals individually 
        res_f = eqs-sig_ref
        res_e = d_lam*eqs_grad - d_eps_p
        res_p = 0.d0!d_lam - d_acp! USELESS TO COMPUTE
        res_x = d_lam*(sig_dev-X)/eps_x - (X+d_X - X_0)
        !PRAGER: res_x = eps_x*d_eps_p - d_X

        ! Compute The merit function with normalized residuals
        merit_fun  = (
     +          (res_f)**2 /dim_sig**2
     +          + sum(res_x*res_x)/dim_sig**2
     +          + sum(res_e*res_e)
     +          + res_p**2
     +          )

c       write(*,*) ">  RES", res_f**2/dim_sig**2 ,
c    +             sum(res_x*res_x)/dim_sig**2,
c    +             sum(res_e*res_e),res_p**2
c       write(*,*) ">  PSI", merit_fun

        RETURN
      end subroutine Residual



C-----------------------------------------------------------------------
C> @details Jacobian
      subroutine Jacobian(sig_dev,X,d_lam,            ! main variables
     +                    h, eqs_grad, eqs_hess,
     +                    eps_x, dim_sig,
     +                    Cinv,
     +                    jac)                  
        implicit none
        real*8        sig_dev(5),X(5),d_lam,            ! main variables
     +                h, eqs_grad(5), eqs_hess(5,5),
     +                eps_x, dim_sig,
     +                Cinv(5,5),
     +                jac(12,12)

        jac = 0.

        jac( 1   , 6   ) = -h
        jac( 1   , 7:11) = eqs_grad

        jac( 2: 6, 1: 5) = Cinv
        jac( 2: 6, 7:11) = eqs_hess*d_lam
        jac( 2: 6,   12) = eqs_grad

        jac(    7,    6) = -1.
        jac(    7,   12) = +1.

        jac(    8,    1) =  -1.
        jac(    9,    2) =  -1.
        jac(   10,    3) =  -1.
        jac(   11,    4) =  -1.
        jac(   12,    5) =  -1.

        jac(    8,    7) =  1. + d_lam/eps_x
        jac(    9,    8) =  1. + d_lam/eps_x
        jac(   10,    9) =  1. + d_lam/eps_x
        jac(   11,   10) =  1. + d_lam/eps_x
        jac(   12,   11) =  1. + d_lam/eps_x

        jac( 8:12,   12) =  (sig_dev - X)/eps_x

        RETURN
      end subroutine Jacobian

