C> @file holmedal19.f
C>    Distortional-kinematic model from Holmedal
C>    @cite holmedal2019 in 2019.
C>    There are 2 backstress variables in addition to the accumulated
C>    plastic strain.
C>    The equivalent stress is
C>    \f[
C>     \bar{\sigma}(\boldsymbol\sigma,\boldsymbol X_1,\boldsymbol X_2,p) =
C>     \left[
C>      (\phi(\boldsymbol\sigma))^q
C>      + f(\boldsymbol X_1, p) \big\langle(2\boldsymbol X_1-\boldsymbol\sigma): \nabla\phi(\boldsymbol X_1) \big\rangle^q
C>      + f(\boldsymbol X_2, p) \big\langle(2\boldsymbol X_2-\boldsymbol\sigma): \nabla\phi(\boldsymbol X_2) \big\rangle^q
C>     \right]^{1/q}
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain. 
C>    \f[
C>     f(\boldsymbol X, p) = 1 -
C>     \left|
C>       \frac{\sigma_R(p)-2\phi(\boldsymbol X)}{\sigma_R(p)}
C>     \right|^{q}
C>    \f]

C> @attention TODO
C>    everything

C-----------------------------------------------------------------------
C> @details 
C>    Desc and ref

C-----------------------------------------------------------------------
C> @param[in,out] SIG
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] D_EPS
C>    Total strain increment
C> @param[in] DT
C>    Time increment.

C> @param[in] params <b>(3)</b> 
C>    | symbol   | name                   | unit | default |
C>    |----------|------------------------|------|---------|
C>    | q        | general exponent       |  -   | 4.      |
C>    | Xsat     | saturation backstress  |  -   | 200     |
C>    | Escale   | strain scale           |  -   | 1.      |

C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.

C> @param[in,out] STATE_VARS  <b>(11)</b> REAL*8 array, dimension (NS)
C>    | Symbol   | Name                         |
C>    |----------|------------------------------|
C>    | aps      | accum plastic strain         |
C>    | X1_1     | first back stress  cpnt 1   |
C>    | X1_2     | first back stress  cpnt 2   |
C>    | X1_3     | first back stress  cpnt 3   |
C>    | X1_4     | first back stress  cpnt 4   |
C>    | X1_5     | first back stress  cpnt 5   |
C>    | X2_1     | second back stress cpnt 1   |
C>    | X2_2     | second back stress cpnt 2   |
C>    | X2_3     | second back stress cpnt 3   |
C>    | X2_4     | second back stress cpnt 4   |
C>    | X2_5     | second back stress cpnt 5   |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/d_eps.
C> @param[out] INFO is INTEGER
C>    - INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    - INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    - INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    - INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    - INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    - INFO = -1 means unsuccesfull finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

C> @todo deal with info, test umat

      subroutine Return_Map(sig, d_eps, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      Implicit none  

      ! (in/out)
      Integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      Real*8   sig(6), d_eps(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      Integer  i, i_max, j, j_max, k, l
      
      
      Real*8 ! State variables
     +     sig_dev(5),    acp,   x1(5),     x2(5),    xi(5),    xj(5),
     +   d_sig_dev(5),  d_acp,  d_X1(5),  d_X2(5),  d_Xi(5),  d_Xj(5),
     +  dd_sig_dev(5), dd_acp, dd_X1(5), dd_X2(5), dd_Xi(5), dd_Xj(5)
      Real*8 
     +  sig_trial(5),
     +  d_eps_p(5), d_sig(6),
     +  sig_ref, eqs, grad(5), hess(5,5), dRdp ! yld fun
      Real*8 tmp1, tmp5(5), tmp16(16)
      Real*8  !other
     +  dim_sig,
     +  residuals(16), psi_0, psi, psi_tol, alpha, eta, beta, ! algo
     +  choldiag(5), cholres(5),
     +  L_inv(5,5), L_(5,5), C_inv(6,6), jac(16,16),
     +  TG_P(5), TG_N(5),
     +  df_dx_dp, dN_dx_dp(5), Ncal(5), Pcal(5)

      Logical PRI, ISNAN, Xi_is_X1

      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      PRI = .True.!.FALSE.!


      Psi_tol = 1.d-20
      Eta = 1.d-1
      Beta = 1.d-4
      i_max = 1000
      j_max = 10

      !=================================================================
      ! UNPACK STATEVARS/PARAMS
      !-----------------------------------------------------------------

      acp = state_vars(1)
      X1 = state_vars(2:6)
      X2 = state_vars(7:11)

      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      ! Elastic guess
      Call dStrain_to_dStress(d_eps, par_EL, d_sig)
      Sig_trial = sig(2:6) + d_sig(2:6)

      d_acp = 0.d0
      d_X1  = 0.d0
      d_X2  = 0.d0

      if (PRI) write(*,*) "▄▄▄▄▄▄▄▄▄▄"
      if (PRI) write(*,"(A, 11F7.2)")  " █ STATEV:", state_vars
      if (PRI) write(*,"(A, 6ES15.1)") " █ D_EPS", d_EPS

      ! Evaluate the merit function for trial stress

      call Reference_Stress(
     +  par_RS, acp, sig_ref)
      call dStress_to_dStrain_dev(
     +  sig_trial-sig_dev,
     +  par_EL, d_eps_p)
      call EQS_AUGMENTED(
     +  sig_trial, acp, X1, X2, 
     +  params,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  1, eqs, grad !! 
     +  )

      ! Tangent operators
      call Tangent_Compliance(par_EL, C_inv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)




c     IF (residuals(1).LE.0.d0) THEN
      IF (eqs.LE.sig_ref) THEN


        alpha = 0.
        sig_dev = sig_trial

        ! State_vars are unchanged
        ! Trial stress was good

        if (PRI) write(*,*) "█ ELASTIC STEP ▗▗▗ with RESIDUALS:"
        if (PRI) write(*,"(16F7.2)") residuals
        if (PRI) write(*,*) "............................"

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

        if (PRI) write(*,*) "█ PLASTIC STEP ▗▗▗"

        ! Initialize newton iteration
        !if (sum(X1*sig_dev).LE.sum(X2*sig_dev)) then
        Xi = (X1-sig_dev)
        Xj = (X2-sig_dev)
        if (sum(Xi**2.d0).LE.sum(Xj**2.d0)) then
          Xi_is_X1 = .True.
          Xi = X1
          Xj = X2
          if (PRI) write(*,*) "█ Xi is X1"
        else
          Xi_is_X1 = .False.
          Xi = X2
          Xj = X1
          if (PRI) write(*,*) "█ Xi is X2"
        endif

        sig_dev = Xi + (sig_trial-Xi)*sig_ref/eqs ! FIRSTGUESS
        !sig_dev = sig_trial

        d_sig_dev = sig_dev - sig(2:6)
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_acp   = sum(d_eps_p*sig_dev)/sig_ref
        ! call Reference_Stress(par_RS, acp, sig_ref)
        d_Xi = (d_sig_dev-Xi)*d_acp/params(3)
        d_Xj = -Xj*d_acp/params(3)

        Xi = Xi + d_Xi 
        Xj = Xj + d_Xj 
        acp = acp+d_acp


        if (PRI) write(*,*) "█ SIG_trial =    ", sig_trial
        if (PRI) write(*,*) "█ GUESSES        "
        if (PRI) write(*,*) "█   SIG_D   =", sig_dev
        if (PRI) write(*,*) "█   d_ACP   =", d_acp
        if (PRI) write(*,*) "█   sig_ref =", sig_ref
        if (PRI) write(*,*) "█   X_i     =", Xi
        if (PRI) write(*,*) "█   X_j     =", Xj

        ! Compute residual

        call Residual(
     +      acp,   sig_dev,   Xi,   Xj, 
     +    d_acp, d_sig_dev, d_Xi, d_Xj, 
     +    sig_trial, dim_sig,
     +    params,
     +    PAR_EL, np_EL,
     +    PAR_ES, np_ES,
     +    PAR_RS, np_RS,
     +    residuals, 
     +    psi_0
     +  )
        if (PRI) write(*,*) "█ PSI_0=", psi_0
        if (PRI) write(*,*) "█ RESIDUALS:"
        if (PRI) write(*,"(16ES9.1)") residuals

        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        ! NEWTON starts

        i = 0
        dd_acp    = 0.d0
        dd_sig_dev= 0.d0
        dd_Xi     = 0.d0
        dd_Xj     = 0.d0

        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))

        !do while (i .LE. i_max-1)
          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory
          i = i+1

          ! ............................................................
          ! NUMERICAL ! Inversion 
          if (PRI) write(*,*) "   █ newton iter ", i, "/", i_max

          call Jacobian(
     +        acp,   sig_dev,   Xi,   Xj, 
     +      d_acp, d_sig_dev, d_Xi, d_Xj, 
     +      sig_trial, dim_sig,
     +      params,
     +      PAR_EL, np_EL,
     +      PAR_ES, np_ES,
     +      PAR_RS, np_RS,
     +      JAC
     +    )


          call linsolve(jac, tmp16, -residuals, 16)

          ! Update increments
          dd_acp    = tmp16(1)
          dd_sig_dev= tmp16(2:6)
          dd_Xi     = tmp16(7:11)
          dd_Xj     = tmp16(12:16)


          ! ............................................................
          ! Residual (U+DU)

          call Residual(
     +      acp+dd_acp, sig_dev+dd_sig_dev, Xi+dd_Xi, Xj+dd_Xj,
     +      d_acp+dd_acp, d_sig_dev+dd_sig_dev, d_Xi+dd_Xi, d_Xj+dd_Xj,
     +      sig_trial, dim_sig,
     +      params,
     +      PAR_EL, np_EL,
     +      PAR_ES, np_ES,
     +      PAR_RS, np_RS,
     +      residuals, 
     +      psi
     +    )

          if (PRI) write(*,*) "   ▐ dd_acp=     ", dd_acp
          if (PRI) write(*,*) "   ▐ PSI", psi
          if (PRI) write(*,*) "   ▐ JAC"
          if (PRI) then
            do j=1,16
              write(*,"(A12)", advance="no") ""
              write(*,"(16F7.2)") jac(j,:)
            enddo
          endif
          if (PRI) write(*,"(A, 16ES10.1)") "    ▐ dd_Xi", dd_Xi
          if (PRI) write(*,"(A, 16ES10.1)") "    ▐ dd_Xj", dd_Xj

          ! ............................................................
          ! Line search 
          alpha = 1.d0
          !psi_0 = psi
          j = 0
          do while ((psi.GT.(1.-2.*beta*alpha)*psi_0) 
     +             .and. (j .LE. j_max))
            ! entering this loop means alpha=1. is not satisfactory.
            ! Let us adjust it and test over and over
            j = j+1
 
            alpha = max(eta*alpha, psi_0/(psi- psi_0*(1-2*alpha)))
 
            !! Compute residual U+alpha*DU
            call Residual(
     +        acp    +d_acp    +alpha*dd_acp,
     +        sig_dev+d_sig_dev+alpha*dd_sig_dev,
     +        Xi     +d_Xi     +alpha*dd_Xi,
     +        Xj     +d_Xj     +alpha*dd_Xj,
     +                d_acp    +alpha*dd_acp,
     +                d_sig_dev+alpha*dd_sig_dev,
     +                d_Xi     +alpha*dd_Xi,
     +                d_Xj     +alpha*dd_Xj,
     +        sig_trial, dim_sig,
     +        params,
     +        PAR_EL, np_EL,
     +        PAR_ES, np_ES,
     +        PAR_RS, np_RS,
     +        residuals, 
     +        psi
     +      )

            if (PRi) write(*,*)"    ▐ linesearch", j, psi, alpha


          enddo

          ! ............................................................
          ! Final merit update

          psi_0 = psi

          ! ------------------------------------------------------------
          ! Final state variables updates
          ! ------------------------------------------------------------

          sig_dev = sig_dev + alpha * dd_sig_dev
          acp     = acp     + alpha * dd_acp
          Xi      = Xi      + alpha * dd_Xi
          Xj      = Xj      + alpha * dd_Xj

          d_acp     = d_acp     + alpha * dd_acp
          d_sig_dev = d_sig_dev + alpha * dd_sig_dev
          d_Xi      = d_Xi      + alpha * dd_Xi
          d_Xj      = d_Xj      + alpha * dd_Xj

          d_sig(1) = alpha * d_sig(1)
          d_sig(2:6) = d_sig_dev


        ! --------------------------------------------------------------
        ! End of NEWTON iteration
        ! --------------------------------------------------------------         NEWTON END
        ENDDO

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------
        ! Build L_INV and inverse
        
        call EQS_AUGMENTED( sig_dev, acp, Xi, Xj, 
     +                      params, PAR_ES, np_ES, PAR_RS, np_RS,
     +                      1, eqs, grad)
        call EQS_AUGMENTED_HESSIAN(sig_dev, acp, Xi, Xj, 
     +                      params, PAR_ES, np_ES, PAR_RS, np_RS,
     +                      HESS)

        L_inv = C_inv(2:6,2:6) + d_acp*hess
        CALL INVERSE(L_inv, 5, L_)

        ! Compute all the weird derivatives.............................
        call CTHelpers(
     +  sig_dev, acp, Xi, Xj, 
     +  dim_sig,
     +  PARAMS,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  df_dx_dp, dN_dx_dp
     +  )

        Pcal = -grad/df_dx_dp
        Ncal = grad + d_acp*dN_dx_dp

        ! Assemble

        tmp1 = 1.d0

        do i=1, 5
          do j=1, 5
            algmod(1+i, 1+j) = sum(L_(i,:)*Ncal)*sum(Pcal*L_(:,j))
            tmp1 = tmp1+Pcal(i)*L_(i,j)*NCal(j)
          enddo
        enddo

        algmod(2:,2:) = L_ - algmod(2:,2:)/tmp1

      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------
      
      sig = sig + d_sig


      STATE_VARS(1)    = acp

      IF (Xi_is_X1) then
        state_vars(2:6)  = Xi
        state_vars(7:11) = Xj
      ELSE
        state_vars(2:6)  = Xj
        state_vars(7:11) = Xi
      ENDIF

      ! ================================================================
      ! Manual WARNINGS for NANs
      ! ----------------------------------------------------------------

      if (PRI) write(*,*) "█ CONCLUDE"
      if (PRI) write(*,"(1ES10.1)")  acp
      if (PRI) write(*,"(6ES10.1)")  sig
      if (PRI) write(*,"(5ES10.1)")  X1
      if (PRI) write(*,"(5ES10.1)")  X2
      if (PRI) write(*,*) "█ RETURNN --------------------------------"
      if (PRI) write(*,*) "▀▀▀▀▀▀▀▀▀"


      RETURN

      end subroutine Return_Map





C#######################################################################



C-----------------------------------------------------------------------
C> @details Residual


      subroutine Residual(
     +    acp,   sig,   Xi,   Xj, 
     +  d_acp, d_sig, d_Xi, d_Xj, 
     +  sig_trial, dim_sig,
     +  PAR_HT,
     +  PAR_EL, np_EL,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  residuals, 
     +  merit_fun
     +)
        implicit none
        integer np_RS, np_ES, np_EL
      ! I/O VARIABLES
        real*8
     +    sig(5),   acp,     Xi(5),   Xj(5),
     +    d_sig(5), d_acp, d_Xi(5), d_Xj(5),
     +    sig_trial(5), dim_sig,
     +    PAR_HT(3),
     +    PAR_EL(np_EL),
     +    PAR_ES(np_ES),
     +    PAR_RS(np_RS), 
     +    residuals(16),
     +    merit_fun

        real*8 sig_ref, X_ref, E_sca,
     +    eqs, deqs_ds(5),
     +    d_eps_p(5)

        ! Unpack =======================================================
        X_ref = PAR_HT(2)
        E_sca = PAR_HT(3)

        ! ==============================================================
        ! Compute residuals individually 
        call Reference_Stress(
     +    par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(
     +    sig_trial-sig,
     +    par_EL, d_eps_p)
        call EQS_AUGMENTED(
     +    sig, acp, Xi, Xj, 
     +    PAR_HT,
     +    PAR_ES, np_ES,
     +    PAR_RS, np_RS,
     +    1, eqs, deqs_ds !! mode 2
     +    )

        residuals(1) = eqs - sig_ref
        residuals(2:6) = -d_eps_p + d_acp * deqs_ds
        residuals(7:11) = -d_Xi + d_acp/E_sca*(X_ref*(sig-Xi)-Xi)
        residuals(12:16) = -d_Xj - d_acp/E_sca*Xj

        ! ==============================================================
        ! Compute The merit function with normalized residuals

        merit_fun =
     +    residuals(1)**2 / dim_sig**2.d0
     +    + sum(residuals(2:6)**2.)
     +    + sum(residuals(7:11)**2.)/ dim_sig**2.d0
     +    + sum(residuals(12:16)**2.)/ dim_sig**2.d0

        !IF (merit_fun.NE.merit_fun) write(*,*) "░░/!\ psi is nan"

        RETURN

      end subroutine Residual

C> @details Jacobian
      subroutine Jacobian(
     +      acp,   sig,   Xi,   Xj, 
     +    d_acp, d_sig, d_Xi, d_Xj, 
     +    sig_trial, dim_sig,
     +    PAR_HT,
     +    PAR_EL, np_EL,
     +    PAR_ES, np_ES,
     +    PAR_RS, np_RS,
     +    JAC
     +)
        implicit none

      ! I/O VARIABLES
        integer np_RS, np_ES, np_EL
        real*8
     +    acp,   sig(5),     Xi(5),   Xj(5),
     +    d_acp, d_sig(5), d_Xi(5), d_Xj(5),
     +    sig_trial(5), dim_sig,
     +    PAR_HT(3),
     +    PAR_EL(np_EL),
     +    PAR_ES(np_ES),
     +    PAR_RS(np_RS), 
     +    JAC(16,16)

      ! INTERNAL VARS
        integer i, j, k
        real*8 res(16), res_0(16),
     +    tmp, U(16), dU(16), diff(16), diff_U(16)

      ! INNIT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        U  = (/acp,   sig,   Xi,   Xj  /)
        dU = (/d_acp, d_sig, d_Xi, d_Xj/)

        diff = 1.d-10
        diff(2:) = diff(2:)*dim_sig
        !write(*,"(16ES8.1)") diff

        CALL Residual(
     +      U(1),  U(2:6), U(7:11),  U(12:16), 
     +     dU(1), dU(2:6),dU(7:11), dU(12:16), 
     +    sig_trial, dim_sig,
     +    PAR_HT, PAR_EL, np_EL, PAR_ES, np_ES, PAR_RS, np_RS,
     +    res_0, tmp
     +  )

        do j=1, 16
          diff_U = 0.d0
          diff_U(j) = diff(j)

          U  = (/acp,   sig,   Xi,   Xj  /) + diff_U
          dU = (/d_acp, d_sig, d_Xi, d_Xj/) + diff_U

          CALL Residual(
     +       U(1),  U(2:6), U(7:11),  U(12:16), 
     +      dU(1), dU(2:6),dU(7:11), dU(12:16), 
     +      sig_trial, dim_sig,
     +      PAR_HT, PAR_EL, np_EL, PAR_ES, np_ES, PAR_RS, np_RS,
     +      res, tmp
     +    )

          JAC(:,j) = (res-res_0)/diff(j)

          if (sum(JAC(:,j)).NE.sum(JAC(:,j))) then
            write(*,*) "░░░nan in jac line ", j
          endif

        enddo


        RETURN
      end subroutine Jacobian


C-----------------------------------------------------------------------
C> @details helper fct
      subroutine EQS_AUGMENTED(
     +  sig, acp, X1, X2, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  mode, eqs, grad
     +  )
        implicit none
        integer mode, np_RS, np_ES
      ! I/O VARIABLES
        real*8
     +    sig(5), acp, X1(5), X2(5),
     +    PAR_HT(2),
     +    PAR_ES(np_ES),
     +    PAR_RS(np_RS), 
     +    eqs, grad(5)
      ! INTERNAL STUFF
        real*8
     +    phi, phi_x, grad_X1(5), grad_X2(5),
     +    psi_1, psi_2, f_1, f_2,
     +    tmp, tmp5(5), tmp55(5,5),
     +    sig_r, 
     +    q

        !***************************************************************
        ! Assign and initialize
        q = PAR_HT(1)
        f_1 = 0.d0
        f_2 = 0.d0

        eqs = 0.d0
        grad = 0.d0

        !***************************************************************
        ! Compute 

        call Equivalent_Stress(
     +      SIG, par_ES, mode, phi, grad, tmp55)
        !if (PHI.GT.0.d0) then
          eqs = phi**q
        !else 

          call Reference_Stress(par_RS, acp, sig_r)

          ! Distortion 1 .................................................
          if (sum(X1*X1).GT.0.d0) then
            call Equivalent_Stress(
     +          X1, par_ES, 1, phi_x, grad_X1, tmp55)
            psi_1 = sum((2*X1 - sig)*grad_X1)
            if (psi_1.GT.0.d0) then
              f_1 = 1.d0-max((sig_r-2*phi_x)/sig_r,0.d0)**q
              eqs = eqs + psi_1**q * f_1
            endif
          else
            psi_1 = 0.d0
            grad_x1 = 0.d0
          endif


          ! Distortion 2..................................................
          if (sum(X2*X2).GT.0.d0) then
            call Equivalent_Stress(
     +          X2, par_ES, 1, phi_x, grad_X2, tmp55)
            psi_2 = sum((2*X2 - sig)*grad_X2)
            if (psi_2.GT.0.d0) then
              f_2 = 1.d0-max((sig_r-2*phi_x)/sig_r,0.d0)**q
              eqs = eqs + psi_2**q * f_2
            endif
          else
            psi_2 = 0.d0
            grad_x2 = 0.d0
          endif

          ! Result .......................................................
          eqs = eqs ** (1.d0/q)
        !endif

        if (EQS.NE.EQS) then
          write(*,*) "░░░░nan in EQS"
          write(*,*) "   ____ sig ",  sig
          write(*,*) "        X_1 ",  x1
          write(*,*) "        X_2 ",  x2
          write(*,*) "   ____ phi ",  phi
          write(*,*) "        psi1",  psi_1 
          write(*,*) "        psi2",  psi_2 
          write(*,*) "   ____ sumgrad",  sum(grad_X1)
          write(*,*) "        sigref",  sig_r
          write(*,*) "        f1, f2", f_1, f_2
          write(*,*) "        OTHERRR", psi_2**q * f_2
          EQS=0.d0 
        endif

      !=================================================================
        IF (mode.EQ.0) RETURN!==========================================
      !=================================================================

        if (eqs.GT.0.d0) then
          grad = (phi/eqs)**(q-1.d0) * grad
          grad = grad
     +          - (psi_1/eqs)**(q-1.d0) * f_1 * grad_X1
     +          - (psi_2/eqs)**(q-1.d0) * f_2 * grad_X2
        else 
          grad = 0.d0
        endif

        if (sum(grad).NE.sum(grad)) then
          grad = 0.d0
          write(*,*) "░░░░nan in EQS_grad"
          write(*,*) "░ sig     =", sig
          write(*,*) "░      X1 =", X1
          write(*,*) "░ grad_x1 =", grad_x1
          write(*,*) "░      X2 =", X2
          write(*,*) "░ grad_x2 =", grad_x1
          
          write(*,*) "░ psi1 =", psi_1
          
          write(*,*) "░ grad =", grad
          write(*,*) "░ eqs =", eqs
        endif


      !=================================================================
        IF (mode.EQ.1) RETURN!==========================================
      !=================================================================

        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"
        write(*,*) "/!\ WARNIIIIIIIIIIIIIIIING, hessian requested"
        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"

      end subroutine EQS_AUGMENTED

C-----------------------------------------------------------------------
C> @details helper fct
      subroutine EQS_AUGMENTED_HESSIAN (
     +  sig, acp, X1, X2, 
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  HESS
     +  )
        implicit none

      ! I/O VARIABLES
        integer mode, np_RS, np_ES
        real*8
     +    sig(5), acp, X1(5), X2(5),
     +    PAR_HT(2),
     +    PAR_ES(np_ES),
     +    PAR_RS(np_RS), 
     +    HESS(5,5)

      ! INTERNAL STUFF
        integer i
        real*8 tmp,
     +    grad(5), grad_0(5), DS(5), d_comp

        call EQS_AUGMENTED(
     +    sig, acp, X1, X2, 
     +    PAR_HT,
     +    PAR_ES, np_ES,
     +    PAR_RS, np_RS,
     +    1, tmp, grad_0
     +  )

        d_comp = 1.d-10
        do i=1, 5
          DS = (/0., 0., 0., 0., 0./)
          DS(i) = d_comp
          call EQS_AUGMENTED(
     +      sig+DS, acp, X1, X2, 
     +      PAR_HT,
     +      PAR_ES, np_ES,
     +      PAR_RS, np_RS,
     +      1, tmp, grad
     +    )
          HESS(:,i) = (grad-grad_0)/d_comp
        enddo

        RETURN

      end subroutine EQS_AUGMENTED_HESSIAN

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      subroutine CTHelpers(
     +  sig_dev, acp, Xi, Xj, 
     +  dim_sig,
     +  PAR_HT,
     +  PAR_ES, np_ES,
     +  PAR_RS, np_RS,
     +  df_dx_dp, dN_dx_dp
     +  )

        implicit none 

        integer mode, np_RS, np_ES
        real*8
     +    sig_dev(5), acp, Xi(5), Xj(5),
     +    dim_sig,
     +    PAR_HT(3),
     +    PAR_ES(np_ES),
     +    PAR_RS(np_RS),
     +    df_dx_dp, dN_dx_dp(5)
 
        integer k
        real*8
     +    eqs_0, grad_0(5), d_eps_scale, d_stress_scale,
     +    eqs , grad(5), dx(5), de,
     +    dXi_dacp(5),
     +    dXj_dacp(5)
        
        ! Scales -------------------------------------------------------
        d_eps_scale = 1.d-15
        d_stress_scale = d_eps_scale * dim_sig
        
        ! Base ---------------------------------------------------------
        call EQS_AUGMENTED( sig_dev, acp, Xi, Xj, 
     +                      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +                      1, eqs_0, grad_0 !! mode 2
     +  )
 
        dXi_dacp = (PAR_HT(2)*(sig_dev-Xi)-Xi)/PAR_HT(3)
        dXj_dacp = (-Xj)/PAR_HT(3)


        call Reference_Stress_Deriv(par_RS, acp, df_dx_dp)

        do k=1, 5

          dx = 0.d0
          dx(k) = d_stress_scale

          !HESS(:,i) = (grad_0 - grad)/d_comp

          call EQS_AUGMENTED(
     +      sig_dev, acp, Xi+dx, Xj, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +      1, eqs, grad)
 

          df_dx_dp = df_dx_dp + (eqs - eqs_0)/d_stress_scale
          dN_dx_dp = dN_dx_dp + (grad_0-grad)/d_stress_scale*dXi_dacp(k)

          call EQS_AUGMENTED(
     +      sig_dev, acp, Xi, Xj+dx, 
     +      PAR_HT, PAR_ES, np_ES, PAR_RS, np_RS,
     +      1, eqs, grad)

          df_dx_dp = df_dx_dp + (eqs - eqs_0)/d_stress_scale
          dN_dx_dp = dN_dx_dp + (grad_0-grad)/d_stress_scale*dXj_dacp(k)

        enddo

        RETURN


      end subroutine
