
C> @file isotropic_gen.f 
C>    Two internal variables named
C>    - accumulated plastic strain denoted \f$p\f$.
C>    - smth that conjugates with X

C> @attention \e Isotropic designates the expansion of
C>    the yield surface. The initial yield surface can be anisotropic.
C>    

C-----------------------------------------------------------------------
C> @details 
C>    This is the return mapping predictor-corrector algorithm for 
C>    solving the closest-point projection in an elastic-plastic 
C>    problem. It is based on a Newton-Raphson method with 
C>    line-search.
C>    The algorithm is based on the paper by Scherzinger, W. M.
C>    @cite scherzinger2017.
C>    This particular implementation is for isotropic elasticity, 
C>    isotropic Voce hardening and Yld2004-18p yield surface.
C>    The implementatoin is detailed by Manik et al. @cite manik2021.

C-----------------------------------------------------------------------
C> @param[in,out] SVEC
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] DSTRAIN
C>    Total strain increment
C> @param[in] DT
C>    Time increment.

C> @param[in] params <b>(0)</b> 

C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.

C> @param[in,out] STATE_VARS  <b>(1)</b> REAL*8 array, dimension (NS)
C>    | Symbol | Name                       |
C>    |--------|----------------------------|
C>    | acp    | accum plastic strain       |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/dstrain.
C> @param[out] INFO is INTEGER
C>    INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    INFO = -1 means unsuccesfull finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

C> @todo deal with info, test umat

      subroutine Return_Map(svec, dstrain, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      implicit none  

      ! (in/out)
      integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      real*8   svec(6), dstrain(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      integer  i, i_max, j, j_max, k, l
      real*8   sig_trial(5), sig_dev(5), acp, d_lam,  ! state vars
     +         d_sig_dev(5), d_acp, dd_lam,         ! increments
     +         d_eps_p(5), d_sig(6),                        ! others
     +         sig_ref, eqs, eqs_grad(5), eqs_hess(5,5), h, !yld fun
     +         res_f, res_e(5), res_p, dim_sig,
     +         L_inv(5,5), C_inv(6,6),
     +         choldiag(5), cholres(5),
     +         psi_0, psi, psi_tol, alpha, eta, beta  ! algo

      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      psi_tol = 1.d-20
      eta = 1.d-1
      beta = 1.d-4
      i_max = 1000
      j_max = 10

      !=================================================================
      ! SPLIT ARRAYS
      !-----------------------------------------------------------------

      acp   = state_vars(1)
      d_eps_p = 0.d0!dstrain(2:6)

      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      call dStrain_to_dStress(dstrain, par_EL, d_sig)
      sig_trial = svec(2:6) + d_sig(2:6)

      ! Evaluate the yield function  f = eqs - sig_ref
      call Reference_Stress(par_RS, acp, sig_ref)
      call Equivalent_Stress(sig_trial, par_ES, 1,
     +                       eqs, eqs_grad, eqs_hess)

      ! Tangent operators
      call Tangent_Compliance(par_EL, C_inv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)

      !write(6,*) "# TEST: ELASTIC"
      !write(*,*) "> START: compare", eqs, " to ", sig_ref, "at", acp

      IF (eqs.LE.sig_ref) THEN
      !IF (.TRUE.) THEN

        alpha = 1.d0
        d_sig_dev = sig_trial - svec(2:6)
        sig_dev = sig_trial
        d_acp     = 0.d0
        dd_lam    = 0.d0

        !write(6,*) "# ELASTIC STEP"

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

        !write(6,*) "# PLASTIC STEP"

        ! Initialize newton iteration

        sig_dev = sig_trial*sig_ref/eqs ! FIRSTGUESS
        !sig_dev = sig_trial

        d_sig_dev = sig_dev - svec(2:6)

        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_lam   = sum(d_eps_p*sig_dev)/sig_ref
        acp     = acp  + d_lam

        d_acp   = d_lam!sum(d_eps_p*sig_dev)/sig_ref
        dd_lam  = d_lam


  
        !! Compute residual

        call Reference_Stress(par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)
c BAP ABOVE  Not in Tomas code, does not change much
        call Equivalent_Stress(sig_dev,!+d_sig_dev, 
     +                         par_ES, 3,
     +                         eqs, eqs_grad, eqs_hess)
        call Residual( sig_dev,d_lam,        ! main variables
     +                 d_eps_p,d_acp,        ! increments
     +                 eqs,eqs_grad,sig_ref, ! other vars
     +                 dim_sig,              ! params
     +                 res_f, res_e, res_p,  ! resisuals
     +                 psi_0)                ! merit function



        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        ! NEWTON START

        i = 0

        !write(*,*) i, "go  Newt-loop with psi0=", psi_0
        !psi_0 = max(psi_0, psi_tol*1.001)
        !write(*,*) i, "go  Newt-loop with psi0=", psi_0

        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))
        !do while (i .LE. i_max)

          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory

          ! ............................................................
          ! Initializations
          i = i+1
  
          ! Get Linv and prepare choleski things
          L_inv = C_inv(2:6,2:6) + d_lam*eqs_hess
          ! (lower triangle of Linv will be modified but the upper
          ! triangle incl. the diagonal will not)
          call chol_decomp(L_inv, 5, choldiag)

          ! ............................................................
          ! Inversion 

          call Reference_Stress(par_RS, acp, sig_ref)
          call Reference_Stress_Deriv(par_RS, acp, h)

          ! Multiplier
          call chol_solve(L_inv, 5, eqs_grad, choldiag, cholres)
          dd_lam = (
     +      res_f + h*res_p
     +      - sum(cholres*res_e)
     +    )/(
     +      h+ sum(cholres*eqs_grad)
     +    )

          d_acp = res_p + dd_lam
          !d_acp = dd_lam

          ! Stress
          call chol_solve(L_inv, 5,
     +                    res_e+eqs_grad*dd_lam,
     +                    choldiag, cholres
     +                    )
          d_sig_dev = - cholres

          ! ............................................................
          ! Residual (U+DU)
          call dStress_to_dStrain_dev(sig_trial-sig_dev-d_sig_dev,
     +                                par_EL, d_eps_p)
          call Equivalent_Stress(sig_dev+d_sig_dev, 
     +                           par_ES, 3,
     +                           eqs, eqs_grad, eqs_hess)
          call Reference_Stress(par_RS, acp+d_acp, sig_ref)
          call Residual(sig_dev+d_sig_dev,       ! main variables
     +                  d_lam+dd_lam,            ! ... + a+dx
     +                  d_eps_p,d_acp,     ! increments
     +                  eqs,eqs_grad, sig_ref,   ! other vars
     +                  dim_sig,                 ! params
     +                  res_f, res_e, res_p,     ! resisuals
     +                  psi)  

          ! ............................................................
          ! Line search 

          alpha=1.
          !write(*,*) i, "G0 LS with  psi=", psi

          j = 0

          do while ((psi .GT. (1.d0-2.d0*beta*alpha)*psi_0) 
     +             .and. (j .LE. j_max))
            ! entering this loop means alpha=1. is not satisfactory.
            ! Let us adjust it and test over and over
            j = j+1

            alpha = max(eta*alpha,
     +                  psi_0*alpha**2/(psi-psi_0*(1.-2.*alpha)))

            !! Compute residual U+alpha*DU
            call dStress_to_dStrain_dev(
     +                        sig_trial-sig_dev-alpha*d_sig_dev,
     +                        par_EL, d_eps_p)
            call Equivalent_Stress(sig_dev+alpha*d_sig_dev, 
     +                             par_ES, 3,
     +                             eqs, eqs_grad, eqs_hess)
            call Reference_Stress(par_RS, acp+alpha*d_acp, sig_ref)
            call Residual(sig_dev+alpha*d_sig_dev,        ! main variables
     +                    d_lam  +alpha*dd_lam,           ! ... + a+dx
     +                    d_eps_p,alpha*d_acp,  ! increments
     +                    eqs,eqs_grad, sig_ref,          ! other vars
     +                           dim_sig,                 ! params
     +                    res_f, res_e, res_p,     ! resisuals
     +                    psi)
            !write(*,*) j, "     in LS with  psi=", psi, "a=",alpha
          enddo
          ! ............................................................
          ! Final merit update

          psi_0 = psi
          !write(*,*) i,"in  Newt-loop with psi0=",psi_0,"alpha=",alpha

          ! --------------------------------------------------------------
          ! Final state variables updates
          ! --------------------------------------------------------------

          sig_dev = sig_dev + alpha * d_sig_dev
          d_lam   = d_lam   + alpha * dd_lam
          acp     = acp     + alpha * d_acp

        enddo                                                                   ! NEWTON STOP

        !d_sig(1) = d_sig(1)
        !d_sig(2:6) = sigd_sig_dev

        !write(*,*) i, "out Newt-loop with  psi=", psi
        !write(*,*) "> END:  compare", eqs, " to ", sig_ref, "at", acp

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------
        
        ! For now algmod = C. Adjust it!

        call Reference_Stress_Deriv(par_RS, acp, h)

        L_inv  = C_inv(2:6,2:6) + eqs_hess*d_lam
        call chol_decomp(L_inv, 5, choldiag)
        call chol_solve(L_inv, 5, eqs_grad, choldiag, cholres)

        call chol_inverse(L_inv, 5, choldiag, algmod(2:6,2:6))

        do k=1,5
          do l=1,5
            algmod(1+k,1+l) =
     +        + algmod(1+k,1+l)
     +        - cholres(k) * cholres(l) / (h+sum(cholres*eqs_grad))
          enddo
        enddo

      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------

      svec(1) = svec(1)+d_sig(1)
      svec(2:6) = sig_dev

      state_vars(1)   = acp

      RETURN

      end subroutine Return_Map





C#######################################################################




C-----------------------------------------------------------------------
C> @brief residual

      subroutine Residual(sig_dev,d_lam,        ! main variables
     +                    d_eps_p,d_acp,        ! increments
     +                    eqs,eqs_grad,sig_ref, ! other vars
     +                    dim_sig,              ! params
     +                    res_f, res_e, res_p,  ! resisuals
     +                    merit_fun)            ! psi
        implicit none

        real*8 sig_dev(5), d_lam,
     +         d_eps_p(5),d_acp,
     +         eqs,eqs_grad(5),sig_ref,      
     +         dim_sig,            
     +         res_f, res_e(5), res_p,
     +         merit_fun                 

        ! Compute residuals individually 
        res_f = eqs-sig_ref
        res_e = d_lam*eqs_grad - d_eps_p
        res_p = 0.d0!d_lam - d_acp!
        ! /!\ RES_P is WRONG, d_lam cannot be compared to
        !     d_acp (incremental d_lam and iterative d_acp)

        ! Compute The merit function with normalized residuals
        merit_fun  = (
     +          res_f**2/dim_sig**2
     +          + sum(res_e*res_e)
     +          + res_p**2
     +          )

      end subroutine Residual

      
C-----------------------------------------------------------------------
C> @brief Jacobian
      subroutine Jacobian(sig_dev,X,d_lam,            ! main variables
     +                    h, eqs_grad, eqs_hess,
     +                    eps_x, dim_sig,
     +                    Cinv,
     +                    jac)                  
        implicit none
        real*8        sig_dev(5),X(5),d_lam,            ! main variables
     +                h, eqs_grad(5), eqs_hess(5,5),
     +                eps_x, dim_sig,
     +                Cinv(5,5),
     +                jac(12,12)

        !jac = 0.

        !jac( 1   , 6   ) = -h
        !jac( 1   , 7:11) = eqs_grad

        !jac( 2: 6, 1: 5) = Cinv
        !jac( 2: 6, 7:11) = eqs_hess*d_lam
        !jac( 2: 6,   12) = eqs_grad

        !jac(    7,    6) = -1.
        !jac(    7,   12) = +1.

        !jac(    8,    1) =  -1.
        !jac(    9,    2) =  -1.
        !jac(   10,    3) =  -1.
        !jac(   11,    4) =  -1.
        !jac(   12,    5) =  -1.

        !jac(    8,    7) =  1. + d_lam/eps_x
        !jac(    9,    8) =  1. + d_lam/eps_x
        !jac(   10,    9) =  1. + d_lam/eps_x
        !jac(   11,   10) =  1. + d_lam/eps_x
        !jac(   12,   11) =  1. + d_lam/eps_x

        !jac( 8:12,   12) =  (sig_dev - X)/eps_x

        RETURN
      end subroutine Jacobian
