
C     This is to simplify the splitting of properties, 
C     see client/parser or generated umat

      subroutine Props_Assign(orig, orig_len,
     +                        dest, dest_len,
     +                        cut)
        implicit none 
        integer  orig_len, cut, dest_len
        real*8   orig(orig_len), dest(dest_len)

        integer  i

        if (dest_len .GE. 0) then
          do i=1,dest_len
            dest(i) = orig(i+cut)
          enddo
        endif

      end subroutine Props_Assign
      
      function ISNaN(x)
        implicit none
        logical ISNaN
        real x
        if (x.EQ.x) then
          ISNAN = .False.
        else
          ISNAN = .True.
        endif

        RETURN
      end function isnan

      subroutine normalize(t, tdim)
        implicit none
        integer tdim
        real*8 t(tdim), norm
        norm = sum(t*t)**.5d0
        if (norm.GT.0.d0) t = t/norm
        return
      end subroutine normalize




c     function ARG_MIN(arr, arr_len)
c       implicit none
c       integer arr_len, ARG_MIN
c       real*8 arr(arr_len)
c       !
c       integer i
c       real*8 val
c       !
c       val=arr(1)
c       ARG_MIN=1
c       !
c       do i=2,arr_len
c         if (arr(i).LT.val) then
c           val = arr(i)
c           ARG_MIN = i
c         endif
c       enddo
c       !
c       RETURN
c     end function ARG_MIN

c     function ARG_MAX(arr, arr_len)
c       implicit none
c       integer arr_len, ARG_MAX
c       real*8 arr(arr_len)
c       !
c       integer i
c       real*8 val
c       !
c       val=arr(1)
c       ARG_MAX=1
c       !
c       do i=2,arr_len
c         if (arr(i).GT.val) then
c           val = arr(i)
c           ARG_MAX = i
c         endif
c       enddo
c       !
c       RETURN
c     end function ARG_MAX


  
      subroutine print_real(num)
        implicit none
        real*8 num
        if  (abs(num).LT.1.d-100) then
          write(*,"(A12)", advance="no") "-"
        else
          write(*,"(ES12.3)", advance="no") num
        endif
      end subroutine print_real

      subroutine print_array(arr, a, b)
        implicit none
        integer a, b, i, j
        real*8 arr(a,b)

        do i=1,a
          do j=1, b
            call print_real(arr(i,j))
          enddo
          write(*,*)
        enddo
      end subroutine print_array
