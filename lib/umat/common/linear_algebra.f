C> @file linear_algebra.f

C> @details
C>    Calculates eigenvalues and eigenvectors of a 3x3 symmetrix 
C>    matrix written in the natural notation as 6x1 vector. Based on 
C>    paper by Scherzinger, W. M. and Dohrmann, C. R., A robust  
C>    algorithm for finding the eigenvalues and eigenvectors of 3x3 
C>    symmetric matrices, Computer Methods in Applied Mechanics and 
C>    Engineering, 2008, DOI: 10.1016/j.cma.2008.03.031
C>
C> @param[in] SVEC is a 6x1 vector representing a symmetric 3x3
C>    tensor in the natural notation
C> @param[in] MODE is integer
C>    MODE = 0 returns only eigenvalues, eigenvectors will not be 
C>    calculated
C>    MODE = else returns both eigenvalues and eigenvectors
C> @param[out] E1, E2, E3 are REAL*8 
C>    three eigenvalues
C> @param[out] EVEC1, EVEC2, EVEC3 
C>    The three eigenvectors

      subroutine eig(svec, mode, e1, e2, e3, evec1, evec2, evec3)
      implicit none  
c     ! in/out
      integer     mode
      real*8      svec(6), e1, e2, e3, evec1(3), evec2(3), evec3(3)
c     ! internal
      integer     i, j, flag, imax, k, m
      real*8      J2, J3, PI, r(3), nr, sdott, Amat(3,3), u1(3), u2(3),
     +            t(3,2), As1(3), As2(3), AR11, AR22, AR23, AR32,   
     +            AR33, s1(3), s2(3), a1, a2, a3, cos3a, ns, w1(3)
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      parameter (PI = 3.14159265359d0)
cc ---------------------- DEFINING SOME CONSTANTS ----------------------
      ov2 = 0.5d0
      ov3 = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt2 = 1.d0/ovsqrt2
      sqrt3 = 1.d0/ovsqrt3

c     build 3x3 deviatoric matrix from svec given in the natural notation
      Amat(1,1) = - ovsqrt6*svec(2) - ovsqrt2*svec(3)
      Amat(2,2) = - ovsqrt6*svec(2) + ovsqrt2*svec(3)
      Amat(3,3) = + 2.d0*ovsqrt6*svec(2)
      Amat(2,3) = ovsqrt2*svec(4)
      Amat(1,3) = ovsqrt2*svec(5)
      Amat(1,2) = ovsqrt2*svec(6)
      Amat(3,2) = Amat(2,3)
      Amat(3,1) = Amat(1,3)
      Amat(2,1) = Amat(1,2)
c
      J2 = 0.d0
      do i=2, 6
          J2 = J2 + svec(i)**2
      end do
      J2 = ov2*J2
c
      J3 = Amat(1,1)*(Amat(2,2)*Amat(3,3)-Amat(3,2)**2)
     +   + Amat(1,3)*(2.d0*Amat(2,1)*Amat(3,2) - Amat(2,2)*Amat(1,3))
     +   - Amat(3,3)*Amat(2,1)**2
c     
      if (J2 .LT. 1.d-30) then
          e1 = 0.d0
          e2 = 0.d0
          e3 = 0.d0
          evec1(1) = 1.d0
          evec1(2) = 0.d0
          evec1(3) = 0.d0
          evec2(1) = 0.d0
          evec2(2) = 1.d0
          evec2(3) = 0.d0
          evec3(1) = 0.d0
          evec3(2) = 0.d0
          evec3(3) = 1.d0
      goto 100
      end if
      cos3a = ov2*J3*(3.d0/J2)**(3.d0/2.d0)
c     to make cos3a within [-1, 1] interval
      cos3a = max(-1.d0, min(1.d0, cos3a))
c
      a1 = ov3*acos(cos3a)
      a3 = a1 + 2.d0*ov3*PI
      a2 = a1 + 4.d0*ov3*PI
c
      if (a1 .LT. PI/6.d0) then
          e1 = 2.d0*sqrt(J2*ov3)*cos(a1)
      else
          e1 = 2.d0*sqrt(J2*ov3)*cos(a3)
      end if
c      
      do i=1, 3
          Amat(i,i) = Amat(i,i) - e1
      end do
c
c     Find the largest column of Amat and store as s1
      ns = 0.d0
      do j=1, 3
          nr = Amat(1,j)**2+Amat(2,j)**2+Amat(3,j)**2
          if (nr .GT. ns) then
              ns = nr
              imax = j
              do i=1, 3
                  s1(i) = Amat(i,j)
              end do
          end if
      end do
c
      do i=1, 3
          s1(i) = s1(i)/sqrt(ns)
      end do
c
      m = 1
      do j=1, 3
          if (j .NE. imax) then
              sdott = s1(1)*Amat(1,j)+s1(2)*Amat(2,j)+s1(3)*Amat(3,j)
              do i=1, 3
                  t(i,m) = Amat(i,j) - sdott*s1(i)
              end do
              m = m+1
          end if
      end do
c
c     Find the largest t column and store as s2
      ns = 0.d0
      do j=1, 2
          nr = t(1,j)**2+t(2,j)**2+t(3,j)**2
          if (nr .GT. ns) then
              ns = nr
              do i=1, 3
                  s2(i) = t(i,j)
              end do
          end if
      end do
c
      do i=1, 3
          s2(i) = s2(i)/sqrt(ns)
      end do
c
c     First eigenvector v1
      evec1(1) = s1(2)*s2(3)-s2(2)*s1(3)
      evec1(2) = s1(3)*s2(1)-s2(3)*s1(1)
      evec1(3) = s1(1)*s2(2)-s2(1)*s1(2)
c
c     Build reduced form of A' matrix (Eq. 22)
      do i=1, 3
          Amat(i,i) = Amat(i,i) + e1
      end do
c
      AR11 = e1
      do i=1, 3
          As1(i) = Amat(i,1)*s1(1) + Amat(i,2)*s1(2) + Amat(i,3)*s1(3)
          As2(i) = Amat(i,1)*s2(1) + Amat(i,2)*s2(2) + Amat(i,3)*s2(3)
      end do
c
      AR22 = s1(1)*As1(1) + s1(2)*As1(2) + s1(3)*As1(3)
      AR23 = s1(1)*As2(1) + s1(2)*As2(2) + s1(3)*As2(3)
      AR32 = s2(1)*As1(1) + s2(2)*As1(2) + s2(3)*As1(3)
      AR33 = s2(1)*As2(1) + s2(2)*As2(2) + s2(3)*As2(3)
c
c     Find the remaining eigenvalues e2, e3 by the Wilkinsons shift
      e2 = ov2*(AR22+AR33) - ov2*sign(1.d0, AR22-AR33)
     +   * sqrt((AR22-AR33)**2 + 4.d0*AR23*AR32)
      e3 = AR22 + AR33 - e2
c
c     returns here if only eigenvalues are required
      if (mode .EQ. 0) goto 100
c
c     Find eigenvectors evec2 and evec3
      do i=1, 3
          Amat(i,i) = Amat(i,i) - e2
      end do
c
      do i=1, 3
          u1(i) = Amat(i,1)*s1(1) + Amat(i,2)*s1(2) + Amat(i,3)*s1(3)
          u2(i) = Amat(i,1)*s2(1) + Amat(i,2)*s2(2) + Amat(i,3)*s2(3)
      end do
c
      nr = u1(1)**2 + u1(2)**2 + u1(3)**2
      ns = u2(1)**2 + u2(2)**2 + u2(3)**2
c     if s1 and s2 are already second and third eigenvectors, then
c     both u1 and u2 and their norms equal zero (ELSE branch)
      if ((nr .GT. 1.d-30) .or. (ns .GT. 1.d-30)) then
          if (nr .GT. ns) then
              do i=1, 3
                  w1(i) = u1(i)/sqrt(nr)
              end do
          else
              do i=1, 3
                  w1(i) = u2(i)/sqrt(ns)
              end do
          end if
          evec2(1) = w1(2)*evec1(3)-evec1(2)*w1(3)
          evec2(2) = w1(3)*evec1(1)-evec1(3)*w1(1)
          evec2(3) = w1(1)*evec1(2)-evec1(1)*w1(2)
c
          evec3(1) = evec1(2)*evec2(3)-evec2(2)*evec1(3)
          evec3(2) = evec1(3)*evec2(1)-evec2(3)*evec1(1)
          evec3(3) = evec1(1)*evec2(2)-evec2(1)*evec1(2)
      else
          do i=1, 3
              evec2(i) = s1(i)
              evec3(i) = s2(i)
          end do
      end if
c
c     adding pressure to get final eigenvalues
100   e1 = e1 + ovsqrt3*svec(1)
      e2 = e2 + ovsqrt3*svec(1)
      e3 = e3 + ovsqrt3*svec(1)
c      
      return
      end subroutine eig

C> details
C>    The CHOL_DECOMP function calculates the Cholesky decomposition
C>    A = L*LT of matrix A, where L is lower-triangular matrix and is
C>    stored in the lower triangle of A except for the diagonal, which
C>    is stored in array CHOLDIAG
C>
c**********************************************************************
      subroutine chol_decomp(A, n, choldiag)
c (IN/OUT) A is REAL*8 array, dimension (n,n)
c          Matrix A must be a positive-definite symmetric matrix and
c          at input it is read from the upper triangle of A. At return,
c          the factorized matrix L will be stored in the lower triangle
c          of A.
c (IN)     N is INTEGER,
c          It is the dimension of A.
c (OUT)    CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A. 
c
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), choldiag(n), sum
c
c     Cholesky factorization of A
      do i=1, n
         do j=i, n
            sum = A(i,j)
            do k=i-1, 1, -1
               sum = sum - A(i,k)*A(j,k)
            end do
            if (i .EQ. j) then
               choldiag(i) = sqrt(sum)
            else
               A(j,i) = sum/choldiag(i)
            end if
         end do
      end do
c 
      return
      end subroutine chol_decomp

C> @details
C>    The CHOL_SOLVE function returns an n-element vector X containing 
C>    the solution to the set of linear equations Ax = b. The Cholesky
C>    factorization of A must be stored in the lower-diagonal of A and
C>    the diagonal factors in CHOLDIAG.
C
c**********************************************************************
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    X is REAL*8, dimension (n)
c          It is the solution of the linear system Ax = b
c
      subroutine chol_solve(A, n, b, choldiag, x)
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), b(n), choldiag(n), x(n), sum
c
c     backsubstitution
      do i=1, n
         sum = b(i)
         do k=i-1, 1, -1
            sum = sum - A(i,k)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do
c
      do i=n, 1, -1
         sum = x(i)
         do k=i+1, n
            sum = sum - A(k,i)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do

      return
      end subroutine chol_solve


C> @details
C>    The CHOL_INVERSE function returns the matrix C which is inverse 
C>    of a positive-definite symmetric matrix A.
      subroutine chol_inverse(A, n, choldiag, C)
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    C is REAL*8, dimension (n,n)
c          It contains inverse of A
c
      implicit none
      integer     i, j, k, n, m
      real*8      A(n,n), b(n), choldiag(n), sum, C(n,n)
c
c     inverse of A by factorized A stored in lower
c     triangle of A and in CHOLDIAG
      do m=1, n
c         create the cartesian basis vectors b    
          do i=1, n
              if (m .EQ. i) then
                  b(i) = 1.d0
              else
                  b(i) = 0.d0
              end if
          end do
c         fill the columns of C with the solutions x=A-1*b
          do i=1, n
             sum = b(i)
             do k=i-1, 1, -1
                sum = sum - A(i,k)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
c
          do i=n, 1, -1
             sum = C(i,m)
             do k=i+1, n
                sum = sum - A(k,i)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
      end do
c      
      return
      end subroutine chol_inverse

C> @details
C>    Computes the outer product of E1 and E2 vector if dimension 3 and 
C>    stores it in vector V in the natural notation
      subroutine outer2vec(e1, e2, v)
      implicit none
      integer     i, j
      real*8      e1(3), e2(3), v(6), tmp(6)
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
c ---------------------- DEFINING SOME CONSTANTS ----------------------
      ov2 = 0.5d0
      ov3 = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt2 = 1.d0/ovsqrt2
      sqrt3 = 1.d0/ovsqrt3    	
      
      tmp(1) = e1(1)*e2(1)
      tmp(2) = e1(2)*e2(2)
      tmp(3) = e1(3)*e2(3)
      tmp(4) = e1(2)*e2(3)
      tmp(5) = e1(1)*e2(3)
      tmp(6) = e1(1)*e2(2)
c     	
      v(1) = ovsqrt3*(tmp(1)+tmp(2)+tmp(3))
      v(2) = ovsqrt6*(2.d0*tmp(3)-tmp(1)-tmp(2))
      v(3) = ovsqrt2*(tmp(2)-tmp(1))
      v(4) = sqrt2*tmp(4)
      v(5) = sqrt2*tmp(5)
      v(6) = sqrt2*tmp(6)
      
      return
      end subroutine outer2vec
c
c
C> @details
C>    Computes the upper triangle of the outer product of E1 and E2 
C>    vectors of dimension 6 given in the natural notation and stores
C>    it in 6x6 matrix M

      subroutine symouter6(e1, e2, M)
	      implicit none
	      integer     i, j
	      real*8      e1(6), e2(6), M(6,6)

       do i=1, 6
     	 	do j=i, 6
     	 		M(i,j) = e1(i)*e2(j)
     	 	end do
       end do

       return
      end subroutine symouter6

C> @details
C>    Function, that computes the dot product of vectors X and Y of 
C>    dimension N
      real*8 function vdot(x, y, n)
c          
      implicit none
      integer     n, i
      real*8      x(n), y(n)

      vdot = 0.d0
      do i = 1, n
          vdot = vdot + x(i)*y(i)
      end do

      RETURN

      end function vdot


C> @details
C>    Compute the matrix-matrix contraction
C> @param[in] A is the LHS square matrix
C> @param[in] B is the RHS square matrix
C> @param[in] order is the system size
C> @param[out] RES is the product result
      subroutine dotprod_mat_mat(A, B, order, res)
        implicit none
        integer order, i, j, k
        real*8 A(order,order), B(order, order), res(order, order)
        res=0.d0
        do i=1,order
          do j=1,order
            do k=1,order
              res(i,j) = res(i,j) + A(i,k)*B(k,j)
            enddo
          enddo
        enddo
        RETURN
      end subroutine dotprod_mat_mat


C> @details
C>    Compute the matrix-vector contraction
C> @param[in] MAT is the LHS square matrix
C> @param[in] VEC is the RHS vector
C> @param[in] order is the system size
C> @param[out] RES is the product result

      subroutine dotprod_mat_vec(mat, vec, order, res)
        implicit none
        integer order, i, j
        real*8 mat(order, order), vec(order), res(order)
        res=0.d0
        do i=1,order
          do j=1,order
            res(i) = res(i) + mat(i,j)*vec(j)
          enddo
        enddo
        RETURN
      end subroutine dotprod_mat_vec

C> @details Gaussian elimination (in-place)
C>    This is from the wiki page pseudocode. Given an augmented matrix
C>    we return the *in-place* row-echelon form.
C> @param[in,out] T is the augmented matrix 
C> @param[in] m is the row number
C> @param[in] n is the number of column
      SUBROUTINE gaussian_elimination(T, m, n)
        implicit none
        integer m, n
        real*8 T(m, n), tmp, tmp_row(n)

        integer h, k, imax, i, j
        h=1
        k=1
        do while ((h.lt.m).and.(k.lt.n))
          ! Find the k-th pivot
          tmp = 0.
          do i=h,m
            if (abs(T(i,k)).GT.tmp) then
              imax = i
              tmp = abs(T(i,k))
            endif
          enddo
          if (tmp.EQ.0.) then 
            ! no pivot in this row, move on
            k = k+1
          else
            ! Swap h and imax rows
            tmp_row   = T(h,:)
            T(h,:)    = T(imax,:)
            T(imax,:) = tmp_row
            ! Then do for all rows below pivot
            do i=h+1,m
              tmp = T(i,k)/T(h,k)
              ! Everything under pivot is zero
              T(i,k) = 0.
              ! The rest of the row gets
              do j=k+1,n
                T(i,j) = T(i,j) - T(h,j)*tmp
              enddo
            enddo
            h = h+1
            k = k+1
          endif
        enddo
        RETURN

      END SUBROUTINE

C> @details Simple linear solver A u = b
C> @param[in] A the linear operator
C> @param[out] u is the solution vector
C> @param[in] b is the RHS
C> @param[in] order is the system size
      SUBROUTINE linsolve(A, u, b, order)
        implicit none
        integer order
        real*8 A(order,order), u(order), b(order)

        integer row, col, i, j, k, n
        real*8 t(order, order+1)

        t(:,1:order)=A
        t(:,order+1)=b

        call gaussian_elimination(t, order, order+1)

        !write(*,*) "After gauss elim"
        !write(*,*) t(1,:)
        !write(*,*) t(2,:)
        !write(*,*) t(3,:)

        b = t(:,order+1)

        u=0.
        do i=0,order-1
          row=order-i
          !write(*,*) row,  t(row,row+1:order)
          !solve the current row
          u(row) = (b(row) -sum(t(row,row+1:order)))/t(row,row)
          t(:,row) = t(:,row)*u(row)
        enddo
        return

      END SUBROUTINE

C> @details Full inverse with Gauss-Jordan elimination
C>    The matrix is augmented by the identity so that all row operations
C>    are stored on the right side.
C>    First get the row echelon form
C>    Then assume  upper triangular form
C>    Finally cancel out the upper triangle by hand

      SUBROUTINE inverse(A, order, INV)
        ! i/o ----------------------------------------------------------
        integer order
        real*8 A(order,order), INV(order,order)

        ! int ----------------------------------------------------------
        integer i, j
        real*8 t(order, 2*order)

        ! Initialization
        T = 0.d0
        do i=1,order
          T(i, order+i) = 1.d0
          do j=1,order
            T(i,j) = A(i,j)
          enddo
        enddo

        ! Row echelon form
        call gaussian_elimination(T, order, 2*order)

        ! Cancel out the upper triangle
        do i=1,order
          do j=i+1,order
            T(i,:) = T(i,:) - T(i,j)/T(j,j) * T(j,:)
            ! Throw a warning if the supposed pivot is zero
            if (abs(T(i,i)).EQ.0.d0) write(*,*) "/!\ zero diag term"
          enddo
          T(i,:) = T(i,:) / T(i,i)
        enddo

        INV = T(:, (ORDER+1):)

        !write(*,*) ""
        !do i=1,order
        !  do j=1,2*order
        !    if (abs(T(i,j)).GT.0.d0) then
        !      write(*,'(ES9.1)', advance='no') T(i,j)
        !    else
        !      write(*,"(A9)", advance="no") "-"
        !    endif
        !  enddo
        !  write(*,*)
        !enddo

        RETURN

      END SUBROUTINE 
