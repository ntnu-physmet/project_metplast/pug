C> @file mises.f The von Mises equivalent stress and its derivatives.
C-----------------------------------------------------------------------
C> @author
C>    - Baptiste Reyne
C-----------------------------------------------------------------------
C> @brief VON MISES
C> @todo details with formulation and reference to paper
C-----------------------------------------------------------------------
C> @param[in] SVEC Stress tensor given in the natural notation
C> @param [in] PARAMS (0) no parameter

C> @param[in] MODE
C>    Controls which of F, GRAD, HESSIAN is to be calculated
C>    - 0  :  only \p PHI is calculated
C>    - 1  :  only \p PHI and \p GRAD is calculated
C>    - else :  \p PHI, \p GRAD and \p HESSIAN are calculated
C> @param[out] PHI The equivalent stress
C>    \f[
C>     \phi = \sqrt{3/2} \sqrt{\boldsymbol s:\boldsymbol s}
C>    \f]
C> @param[out] GRAD The first derivative
C>    \f[
C>     \phi^2 = \frac32 s_{ij}s_{ij}   \\
C>     2\phi\frac{\partial\phi}{\partial s_{kl}} = \frac32 (2s_{kl}) \\
C>     \frac{\partial\phi}{\partial s_{kl}} = \frac32 \frac{s_{kl}}{ \phi  }
C>    \f]
C> @param[out] HESSIAN The second derivative
C>    \f[
C>     \phi\frac{\partial\phi}{\partial s_{kl}} = \frac32 s_{kl} \\
C>     \phi\frac{\partial\phi}{\partial s_{kl}\partial s_{pq}} 
C>     + \frac{\partial\phi}{\partial s_{kl}}\frac{\partial\phi}{\partial s_{kl}}
C>     = \frac32 P^D_{klpq}\\
C>     \frac{\partial\phi}{\partial s_{kl}\partial s_{pq}} 
C>     =
C>      \bigg(
C>       \frac32 P^D_{klpq} - \frac94 \frac{s_{kl}s_{pq}}{ \phi^2  }
C>      \bigg)/\phi
C>    \f]
C-----------------------------------------------------------------------

      SUBROUTINE Equivalent_Stress(
     + svec, params, mode, phi, grad, hessian)

        IMPLICIT NONE
        
        integer     mode
        real*8      svec(5), params(0), phi, grad(5), hessian(5,5)

        integer     i, j
        
        phi = sqrt(sum(svec*svec)*1.5)

        IF (mode.EQ.0) RETURN

        if (phi.eq.0.d0) then
          grad = 0.d0
        else
          grad = svec/phi*1.5
        endif

        IF (mode.EQ.1) RETURN

        hessian = 0.
        if (phi.gt.0.d0) then
          do i=1,5
            hessian(i,i) = 1.5
            do j=1,5
              hessian(i,j) = hessian(i,j)
     +                     - svec(i)*svec(j)/(phi**2) * 2.25
            enddo
          enddo
          hessian = hessian/phi
        endif
        
        RETURN

      END SUBROUTINE Equivalent_Stress
